using System.Collections.Generic;
using Godot;
using System.Threading.Tasks;
using System.Linq;
using System.Text;
using System;
using Nakama;

namespace NakamaWebrtcP2P.Logic {
public class OnlineMatch : Node {

	public static event Action<Dictionary<string, P2PPlayer>> OnMatchReady = delegate { };
	public static event Action<string> OnMatchCreated = delegate { };
	public event Action<P2PPlayer> OnPlayerLeft = delegate { };

	enum MatchOpCode {
		WEBRTC_PEER_METHOD = 9001,
		JOIN_SUCCESS = 9002,
		JOIN_ERROR = 9003,
	}

	private int minPlayers = 2;
	private int maxPlayers = 2;
	private string clientVersion = "dev";
	private Godot.Collections.Dictionary ice_servers = new Godot.Collections.Dictionary { 
		//{"urls", new List<string>() {"stun:stun.l.google.com:19302"}}
		{"urls", "stun:stun.l.google.com:19302"}
	};
	
	enum NetworkRelay {
		AUTO,
		FORCED,
		DISABLED
	}
	private NetworkRelay useNetworkRelay = NetworkRelay.AUTO;

	enum MatchMode {
		NONE = 0,
		CREATE = 1,
		JOIN = 2,
		MATCHMAKER = 3,
	}

	private MatchMode matchMode;

	enum JoinErrorReason {
		MATCH_HAS_ALREADY_BEGUN,
		MATCH_IS_FULL
	}
		
	//Nakama variables:
	private Nakama.ISocket nakamaSocket;
	private string matchId;
	private string mySessionId;
	private string matchmakerTicket;

	//WebRTC variables:
	private WebRTCMultiplayer webrtcMultiplayer;
	private Dictionary<string, WebRTCPeerConnection> webrtcPeers = new Dictionary<string, WebRTCPeerConnection>();
	private Dictionary<string, bool> webrtcPeersConnected = new Dictionary<string, bool>();
	
	private Dictionary<string, P2PPlayer> players = new Dictionary<string, P2PPlayer>();
	private int nextPeerId;

	private Node myNode;
		
	enum MatchState {
		LOBBY = 0,
		MATCHING = 1,
		CONNECTING = 2,
		WAITING_FOR_ENOUGH_PLAYERS = 3,
		READY = 4,
		PLAYING = 5,
	}

	private MatchState matchState;

	public OnlineMatch(Node myNode) {
		this.myNode = myNode;
	}

	public async Task CreateMatch(Nakama.ISocket _socket) {
		await Leave();
		SetNakamaSocket(_socket);
		matchMode = MatchMode.CREATE;

		var data = await nakamaSocket.CreateMatchAsync();
		OnNakamaMatchCreated(data);
	}

	private void OnNakamaMatchCreated(Nakama.IMatch data) {
		matchId = data.Id;
		mySessionId = data.Self.SessionId;
		var myPlayer = new P2PPlayer(data.Self, 1);
		players[mySessionId] = myPlayer;
		nextPeerId = 2;
		
		webrtcMultiplayer.Initialize(1);
		myNode.GetTree().NetworkPeer = webrtcMultiplayer;
		OnMatchCreated(matchId);
	}

	public async Task JoinMatch(ISocket _socket, string matchId) {
		Leave();
		SetNakamaSocket(_socket);
		matchMode = MatchMode.JOIN;

		var data = await nakamaSocket.JoinMatchAsync(matchId);
		OnNakamaMatchJoin(data);	

	}

	public async Task Leave() {
		if(webrtcMultiplayer != null) {
			webrtcMultiplayer.Close();
			myNode.GetTree().NetworkPeer = null;
		}
		if(nakamaSocket != null) {
			if(matchId != "") {
				await nakamaSocket.LeaveMatchAsync(matchId);
			}
			if(matchmakerTicket != "") {
				await nakamaSocket.RemoveMatchmakerAsync(matchmakerTicket);
			}
		}	
		InitializeVariables();
		GD.Print("Left match");
	}

	private void InitializeVariables() {
		mySessionId = "";
		matchId = "";
		matchmakerTicket = "";
		CreateWebRTCMultiplayer();
		webrtcPeers = new Dictionary<string, WebRTCPeerConnection>();
		webrtcPeersConnected = new Dictionary<string, bool>();
		players = new Dictionary<string, P2PPlayer>();
		nextPeerId = 1;
		matchState = MatchState.LOBBY;
		matchMode = MatchMode.NONE;
	}

	private void CreateWebRTCMultiplayer() {
		if(webrtcMultiplayer != null) {
			webrtcMultiplayer.Disconnect("peer_connected", this, nameof(OnWebRTCPeerConnected));
			//TODO: Peer diconnected
		}
		webrtcMultiplayer = new WebRTCMultiplayer();
		webrtcMultiplayer.Connect("peer_connected", this, nameof(OnWebRTCPeerConnected));
			//TODO: Peer diconnected
	}

	private void SetNakamaSocket(Nakama.ISocket socket) {
		if(nakamaSocket != null) {
			nakamaSocket.ReceivedMatchState -= OnNakamaMatchState;
			nakamaSocket.ReceivedMatchPresence -= OnNakamaMatchPresence;
			nakamaSocket.ReceivedError -= PrintNakamaError;
		}
		nakamaSocket = socket;
		nakamaSocket.ReceivedMatchState += OnNakamaMatchState;
		nakamaSocket.ReceivedMatchPresence += OnNakamaMatchPresence;
		nakamaSocket.ReceivedError += PrintNakamaError;
	}

	private void PrintNakamaError(Exception e) {
		GD.Print(e.ToString());
	}

	private void OnNakamaMatchState(Nakama.IMatchState data) {
		var jsonResult = JSON.Parse(Encoding.UTF8.GetString(data.State));
		var content = (Godot.Collections.Dictionary) jsonResult.Result;
		if(jsonResult.Error != Error.Ok) {
			GD.Print("Nakama Match state Json parse error");
			return;
		}

		if(data.OpCode == (long) MatchOpCode.WEBRTC_PEER_METHOD) {
			if((string) content["target"] == mySessionId) {
				var sessionId = data.UserPresence.SessionId;
				if(!webrtcPeers.ContainsKey(sessionId)) {
					return;
				}
				var webrtcPeer = webrtcPeers[sessionId];
				switch(content["method"]) {
					case "set_remote_description":
						webrtcPeer.SetRemoteDescription((string) content["type"], (string)content["sdp"]);
						break;
					case "add_ice_candidate":
						if(WebRTCCheckIceCanditate((string) content["name"])) {
							webrtcPeer.AddIceCandidate((string) content["media"],  Convert.ToInt32(content["index"]) , (string) content["name"]);
						}
						GD.Print("adding ice candidate");
						break;
				}
			}
		} else if(data.OpCode == (long) MatchOpCode.JOIN_SUCCESS && matchMode == MatchMode.JOIN) {
			var hostClientVersion = content["client_version"];
			if(clientVersion != (string) hostClientVersion) {
				Leave();
				//TODO Error codes
				return;
			}
			var contentPlayers = P2PPlayer.UnserializePlayers((Godot.Collections.Dictionary) content["players"]);
			foreach(var entry in contentPlayers) {
				if(!players.ContainsKey(entry.Key)) {
					players[entry.Key] = entry.Value;
					WebRTCConnectPeer(entry.Value);
					if(entry.Key == mySessionId) {
						webrtcMultiplayer.Initialize(entry.Value.PeerId);	
						myNode.GetTree().NetworkPeer = webrtcMultiplayer;

						//TODO: Signal
					}
				}
			}
		} else if(data.OpCode == (long) MatchOpCode.JOIN_ERROR) {
			if(content["target"] == mySessionId) {
				Leave();
				//TODO: emit signal
				return;
			}
		}
	}

	private void OnWebRTCPeerConnected(int peerId) {
		GD.Print("Tryng to connect peer");
		foreach(var entry in players) {
			if(entry.Value.PeerId == peerId) {
				webrtcPeersConnected[entry.Value.SessionId] = true;
				GD.Print("WebRTC peer connected: " + peerId.ToString());
				// TODO: Maybe emit event here
			}
		}

		if(webrtcPeersConnected.Count == players.Count - 1) {
			if(players.Count >= minPlayers) {
				matchState = MatchState.READY;	
				OnMatchReady(players);
			} else {
				matchState = MatchState.WAITING_FOR_ENOUGH_PLAYERS;
			}
		}
	}

	private async Task OnNakamaMatchmakerMatched(Nakama.IMatchmakerMatched data) {
		GD.Print("Matchmake matched");
		mySessionId = data.Self.Presence.SessionId;

		foreach(var user in data.Users) {
			players[user.Presence.SessionId] = new P2PPlayer(user.Presence, 0);
		}

		var session_ids = players.Keys.ToList();
		session_ids.Sort();
		foreach(var id in session_ids) {
			players[id].SetPeerId(nextPeerId);
			nextPeerId += 1;
		}
		webrtcMultiplayer.Initialize(players[mySessionId].PeerId);
		myNode.GetTree().NetworkPeer = webrtcMultiplayer;

		//TODO: Maybe emit Signals here

		var result = await nakamaSocket.JoinMatchAsync(data);
		OnNakamaMatchJoin(result);
	}

	private void OnNakamaMatchPresence(Nakama.IMatchPresenceEvent data) {
		GD.Print("Received Nakama Match Presence");
		foreach(var user in data.Joins) {
			if(user.SessionId == mySessionId)
				continue;
			else if(matchMode == MatchMode.CREATE) {
				if(matchState == MatchState.PLAYING) {
					nakamaSocket.SendMatchStateAsync(matchId, (long) MatchOpCode.JOIN_ERROR, JSON.Print(new Godot.Collections.Dictionary {
						{"target", user.SessionId},
						{"code", JoinErrorReason.MATCH_HAS_ALREADY_BEGUN},
						//TODO Maybe add a reason here
					}));
				} else if(players.Count < maxPlayers) {
					var newPlayer = new P2PPlayer(user, nextPeerId);
					nextPeerId += 1;
					GD.Print("nextPeerId: " + nextPeerId);
					players[user.SessionId] = newPlayer;
					//TODO maybe emit join event
					
					nakamaSocket.SendMatchStateAsync(matchId, (long) MatchOpCode.JOIN_SUCCESS, JSON.Print(new Godot.Collections.Dictionary {
						{"players", P2PPlayer.SerializePlayers(players)},
						{"client_version", clientVersion}
					}));

					WebRTCConnectPeer(newPlayer);
				} else {
					//Were full
					nakamaSocket.SendMatchStateAsync(matchId, (long) MatchOpCode.JOIN_ERROR, JSON.Print(new Godot.Collections.Dictionary {
						{"target", user.SessionId },
						{"code", JoinErrorReason.MATCH_IS_FULL},
						//TODO maybe add a reason
					}));
				}
			} else if(matchMode == MatchMode.MATCHMAKER) {
				WebRTCConnectPeer(players[user.SessionId]);
			}
		}
		foreach(var user in data.Leaves) {
			if(user.SessionId == mySessionId)
				continue;
			if(!players.ContainsKey(user.SessionId))
				continue;

			var player = players[user.SessionId];
			WebRTCDisconnectPeer(player);

			//HOst disconnected close session
			if(player.PeerId == 1) {
				Leave();
				GD.Print("Host disconnected");
				OnPlayerLeft(player);
				return;
				//TODO error code emission
			} else {
				players.Remove(user.SessionId);
				OnPlayerLeft(player);
				if(players.Count < minPlayers) {
					if(matchState == MatchState.READY) {
						matchState = MatchState.WAITING_FOR_ENOUGH_PLAYERS;
					}
				} else {
					if(webrtcPeersConnected.Count == players.Count - 1) {
						matchState = MatchState.READY;
					}
				}
				//TODO maybe signal
			}
		}
	}

	private void WebRTCDisconnectPeer(P2PPlayer player) {
		var webrtcPeer = webrtcPeers[player.SessionId];
		webrtcPeer.Close();
		webrtcPeers.Remove(player.SessionId);
		webrtcPeersConnected.Remove(player.SessionId);
	}


	public async Task StartMatchmaking(Nakama.ISocket socket) {
		await Leave();
		GD.Print("Started Matchmaking");
		SetNakamaSocket(socket);
		matchMode = MatchMode.MATCHMAKER;
		
		socket.ReceivedMatchmakerMatched += async matchmakerMatched => {
			await OnNakamaMatchmakerMatched(matchmakerMatched);
		};

		var minPlayers = 2;
		var maxPlayers = 2;
		var query = "";

		var matchmakingTicket = await socket.AddMatchmakerAsync(query, minPlayers, maxPlayers);
	}

	private void OnNakamaMatchJoin(Nakama.IMatch data) {
		matchId = data.Id;
		mySessionId = data.Self.SessionId;

		if(matchMode == MatchMode.JOIN) {
			//TODO maybe emit signal
		} else if(matchMode == MatchMode.MATCHMAKER) {
			foreach(var user in data.Presences) {
				if(user.SessionId == mySessionId)
					continue;
				WebRTCConnectPeer(players[user.SessionId]);
			}	
		}
	}

	public void WebRTCConnectPeer(P2PPlayer player) {
		GD.Print("Starting Peer Connection");
		if(webrtcPeers.ContainsKey(player.SessionId))
			return;

		if(matchState != MatchState.PLAYING) {
			matchState = MatchState.CONNECTING;
		}
		
		new WebRTCPeerConnection().Initialize();
		var webrtcPeer = new WebRTCPeerConnection();

		webrtcPeer.Connect("session_description_created", this, nameof(OnWebrtcPeerSessionDescriptionCreated), 
				new Godot.Collections.Array {player.SessionId});
		webrtcPeer.Connect("ice_candidate_created", this, nameof(OnWebrtcPeerIceCandidateCreated), 
				new Godot.Collections.Array {player.SessionId});

		webrtcPeer.Initialize(
			new Godot.Collections.Dictionary { 
				{ "iceServers", new Godot.Collections.Array { 
					new Godot.Collections.Dictionary { { "urls", new Godot.Collections.Array { "stun:stun.l.google.com:19302" } } } } 
				} 
			}
		);


		webrtcPeers.Add(player.SessionId, webrtcPeer);

		webrtcMultiplayer.AddPeer(webrtcPeer, player.PeerId, 0);

		if(mySessionId.CompareTo(player.SessionId) < 0) {
			var result = webrtcPeer.CreateOffer();
			if(result != Error.Ok) {
				GD.Print(result);
			}
		}
	}

	private bool WebRTCCheckIceCanditate(string name) {
		if(useNetworkRelay == NetworkRelay.AUTO)
			return true;

		var isRelay = name.Contains("typ relay");
		if(useNetworkRelay == NetworkRelay.FORCED)
			return isRelay;
		return !isRelay;
	}

	private void OnWebrtcPeerIceCandidateCreated(string media, int index, string name, string session_id) {
		GD.Print("ice candidate created");
		if(!WebRTCCheckIceCanditate(name))
			return;

		// Send this data to the peer so they can call .add_ice_candidate()
		nakamaSocket.SendMatchStateAsync(matchId, (int)MatchOpCode.WEBRTC_PEER_METHOD, JSON.Print( new Godot.Collections.Dictionary {
			{"method",  "add_ice_candidate"},
			{"target",  session_id},
			{"media", media},
			{"index",  index},
			{"name", name},
		})
		);
	}


	private void OnWebrtcPeerSessionDescriptionCreated(string type, string sdp, string sessionId) {
		GD.Print("SessionDescription created");
		var webrtcPeer = webrtcPeers[sessionId];
		webrtcPeer.SetLocalDescription(type, sdp);
		nakamaSocket.SendMatchStateAsync(matchId, (int) MatchOpCode.WEBRTC_PEER_METHOD,
			JSON.Print(new Godot.Collections.Dictionary {
				{"method",  "set_remote_description"},
				{"target", sessionId},
				{"type",  type},
				{"sdp",  sdp}
			})
		);
	}



}
}
