using System;
using System.Threading.Tasks;
using Nakama;
using Godot;


namespace NakamaWebrtcP2P.Logic {
public class Authenticator : Node {
	public static event Action OnAuthenticate = delegate { };
	public static event Action<Nakama.ApiResponseException> OnAuthError = delegate { };

	private static OnlineMatch onlineMatch;
	public static OnlineMatch OnlineMatch => onlineMatch;

	private static Nakama.ISession session;
	private static Client client;
	private static Nakama.ISocket socket;
	public static Nakama.ISocket NakamaSocket => socket;

	private static string scheme = "http";
	private static string host = "127.0.0.1";
	private static int port = 7350;
	private static string serverKey = "defaultkey";

	private static string emailTmp;
	private static string passwordTmp;
	private static Node autoloadTmp;

	public static void ConfigureNakama(string _host, int _port, string _serverKey, string _scheme) {
		GD.Print(_host);
		if(host != "127.0.0.1" || serverKey != "defaultkey") {
			throw new System.Exception("Trying to ConfigureNakama twice.");
		}
		host = _host;
		port = _port;
		serverKey = _serverKey;
		scheme = _scheme;
	}

	public static async Task<Nakama.ISession> GetSession() {
		if(session.IsExpired) {
			await AuthenticateEmailAndCreate(emailTmp, passwordTmp, null, false, autoloadTmp);		
		}
		return session;
	}

	public static async Task AuthenticateEmailAndCreate(string email, string password, string username, bool create, Node autoload) {
		GD.Print("Authenticating");
		emailTmp = email;
		passwordTmp = password;
		autoloadTmp = autoload;

		var httpAdapter = new GodotHttpAdapter();
		autoload.AddChild(httpAdapter);

		client = new Client(scheme, host, port, serverKey, httpAdapter);
		client.Timeout = 10;
		client.GlobalRetryConfiguration = new RetryConfiguration(
			baseDelayMs: 500,
			jitter: RetryJitter.FullJitter,
			listener: (x,y) => GD.Print("Retry"),
			maxRetries: 4
		);

		try { 
			session = await client.AuthenticateEmailAsync(email, password, username, create);
			GD.Print("After sessione");
			OnAuthenticate();
		} catch (Nakama.ApiResponseException e) {
			GD.Print(e.ToString());
			OnAuthError(e);
		}

		var websocketAdapter = new GodotWebSocketAdapter();
		autoload.AddChild(websocketAdapter);

		socket = Socket.From(client, websocketAdapter);

		socket.ReceivedError += (e) => {
			GD.Print(e.ToString());
		};
		socket.Connected += () => {
			GD.Print("Socket connected.");
		};
		socket.Closed += () => {
			GD.Print("Socket closed.");
		};

		await socket.ConnectAsync(session);
		if(onlineMatch == null)
			onlineMatch = new OnlineMatch(autoloadTmp);
	}

	public static void Disconnect() {
		socket.CloseAsync();
	}
	
		
}
}
