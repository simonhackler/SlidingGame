using Nakama;
using System.Collections.Generic;
using Godot;
using System;

namespace NakamaWebrtcP2P.Logic {
public class P2PPlayer {

	public string SessionId {get; private set;}
	public int PeerId {get; private set;}
	public string Username {get; private set;}

	public P2PPlayer(string SessionId, int PeerId, string username) {
		this.SessionId = SessionId;
		this.PeerId = PeerId;
		this.Username = username;
	}

	public P2PPlayer(IUserPresence presence, int peerId) {
		this.SessionId = presence.SessionId;
		this.Username = presence.Username;
		this.PeerId = peerId;
	}

	public void SetPeerId(int peerId) {
		this.PeerId = peerId;
	}

	public static P2PPlayer FromDict(Godot.Collections.Dictionary data) {
		return new P2PPlayer((string) data["session_id"], Convert.ToInt32(data["peer_id"]),(string) data["username"]);
	}

	public static Godot.Collections.Dictionary ToDict(P2PPlayer player) {
		return new Godot.Collections.Dictionary {
			{"session_id", player.SessionId},
			{"username", player.Username},
			{"peer_id", player.PeerId}
		};
	}

	public static Godot.Collections.Dictionary SerializePlayers(Dictionary<string, P2PPlayer> players) {
		var result = new Godot.Collections.Dictionary();
		foreach(var entry in players) {
			result.Add(entry.Key, ToDict(entry.Value));
		}
		return result;
	}

	public static Dictionary<string, P2PPlayer> UnserializePlayers(Godot.Collections.Dictionary players) {
		var result = new Dictionary<string, P2PPlayer>();
		foreach(System.Collections.DictionaryEntry entry in players) {
			GD.Print(entry.Key.GetType());
			GD.Print(entry.Value.GetType());
			//GD.Print(players[(string) entry].GetType());
			result.Add((string) entry.Key, FromDict((Godot.Collections.Dictionary) players[(string) entry.Key]));
		}
		return result;
	}


}
}
