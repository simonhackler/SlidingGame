using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Linq;

public enum VALIDATION_MESSAGE {
	SUCCESFULL,
	NOT_A_VALID_EMAIL,
	USERNAME_TO_LONG,
	USERNAME_TO_SHORT,
	USERNAME_INVALID_CHARACTER,
	PASSWORD_TO_SHORT,
	PASSWORD_TO_LONG,
	PASSWORD_WHITESPACE,
}

public class Validator {

	public static string CreateMessage(VALIDATION_MESSAGE mesg) {
		switch(mesg) {
			case VALIDATION_MESSAGE.SUCCESFULL:
				return "";
			case VALIDATION_MESSAGE.NOT_A_VALID_EMAIL:
				return "Please enter a valid e-mail";
			case VALIDATION_MESSAGE.USERNAME_TO_LONG:
				return "Please enter a username with 30 or less characters";
			case VALIDATION_MESSAGE.USERNAME_TO_SHORT:
				return "Please enter a username with 3 or more characters";
			case VALIDATION_MESSAGE.USERNAME_INVALID_CHARACTER:
				return "Please enter a username without special characters";
			case VALIDATION_MESSAGE.PASSWORD_TO_SHORT:
				return "Please enter a password with 8 or more characters";
			case VALIDATION_MESSAGE.PASSWORD_TO_LONG:
				return "Please enter a password with 72 or less characters";
			case VALIDATION_MESSAGE.PASSWORD_WHITESPACE:
				return "Please enter a password without whitespace";
			default:
				throw new System.Exception("Validation mesg does not exist");
		}

	}

	public static VALIDATION_MESSAGE ValidateEmail(string email) {
		if (string.IsNullOrWhiteSpace(email))
				return VALIDATION_MESSAGE.NOT_A_VALID_EMAIL;
		try {
			// Normalize the domain
			email = Regex.Replace(email, @"(@)(.+)$", DomainMapper,
									RegexOptions.None, TimeSpan.FromMilliseconds(200));

			// Examines the domain part of the email and normalizes it.
			string DomainMapper(Match match) {
					// Use IdnMapping class to convert Unicode domain names.
				var idn = new IdnMapping();
					// Pull out and process domain name (throws ArgumentException on invalid)
				string domainName = idn.GetAscii(match.Groups[2].Value);
				return match.Groups[1].Value + domainName;
			}
		} catch (RegexMatchTimeoutException e) {
			return VALIDATION_MESSAGE.NOT_A_VALID_EMAIL;
		} catch (ArgumentException e) {
			return VALIDATION_MESSAGE.NOT_A_VALID_EMAIL;
		}

		try {
			if(Regex.IsMatch(email,
				@"^[^@\s]+@[^@\s]+\.[^@\s]+$",
				RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250))) {
					return VALIDATION_MESSAGE.SUCCESFULL;
				} else {
					return VALIDATION_MESSAGE.NOT_A_VALID_EMAIL;
				}
		}
		catch (RegexMatchTimeoutException) {
			return VALIDATION_MESSAGE.NOT_A_VALID_EMAIL;
		}
	}

	public static VALIDATION_MESSAGE ValidateUsername(string username) {
		if(string.IsNullOrWhiteSpace(username))
			return VALIDATION_MESSAGE.USERNAME_TO_SHORT;
		if(username.Length < 3)
			return VALIDATION_MESSAGE.USERNAME_TO_SHORT;
		if(username.Length > 30)
			return VALIDATION_MESSAGE.USERNAME_TO_LONG;
		try {
			if(Regex.IsMatch(username,
					@"^\w+$",
					RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250))) {
				return VALIDATION_MESSAGE.SUCCESFULL;		
			} else {
				return VALIDATION_MESSAGE.USERNAME_INVALID_CHARACTER;
			}
		}
		catch (RegexMatchTimeoutException) {
				return VALIDATION_MESSAGE.USERNAME_INVALID_CHARACTER;
		}
	}

	public static VALIDATION_MESSAGE ValidatePassword(string password) {
		if(string.IsNullOrWhiteSpace(password))
			return VALIDATION_MESSAGE.PASSWORD_TO_SHORT;
		if(password.Length < 8)
			return VALIDATION_MESSAGE.PASSWORD_TO_SHORT;
		if(password.Length > 72)
			return VALIDATION_MESSAGE.PASSWORD_TO_LONG;
		try {
			if(Regex.IsMatch(password,
					@"^\S+$",
					RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250))) {
				return VALIDATION_MESSAGE.SUCCESFULL;
			} else {
				return VALIDATION_MESSAGE.PASSWORD_WHITESPACE;
			}
		}
		catch (RegexMatchTimeoutException) {
				return VALIDATION_MESSAGE.PASSWORD_WHITESPACE;
		}
	}

}
