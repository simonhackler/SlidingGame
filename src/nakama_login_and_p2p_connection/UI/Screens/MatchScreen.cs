using Godot;
using NakamaWebrtcP2P.Logic;

namespace NakamaWebrtcP2P.UI {
public class MatchScreen : Screen {

	private RichTextLabel matchCode;


	public override void _Ready() {
		OnlineMatch.OnMatchCreated += DisplayCode;
		var parent = GetNode("Panel").GetNode("HBoxContainer");
		matchCode = parent.GetNode<RichTextLabel>("MatchCode");
		parent.GetNode<Button>("CopyClipboardButton").Connect("pressed", this, nameof(ClipboardButtonPressed));
	}	

	public override void Deactivate() {
		this.Visible = false;
		Authenticator.OnlineMatch.Leave();
	}

	private void ClipboardButtonPressed() {
		OS.Clipboard = matchCode.Text;
	}

	private void DisplayCode(string code) {
		matchCode.Text = code;
		GD.Print(code);
		matchCode.BbcodeText = "\n[center]" + code + "[/center]";
	}

}
}
