using Godot;
using System;
using NakamaWebrtcP2P.Logic;

namespace NakamaWebrtcP2P.UI {
public class MatchmakingScreen: Screen
{

	public override void Deactivate() {
		this.Visible = false;
		_ = Authenticator.OnlineMatch.Leave();
	}

	public override void _Ready()
	{
	}

}
}
