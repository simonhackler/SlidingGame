using Godot;
using System;
using NakamaWebrtcP2P.Logic;
using System.Threading.Tasks;
using Nakama;

namespace NakamaWebrtcP2P.UI {
public class ConnectionScreen : Screen
{

	public event Action OnLoginSuccesfull = delegate {};

	private const string LINE_EDIT_VALID = "LineEditValid";
	private const string LINE_EDIT_ERROR = "LineEditError";

	private LineEdit emailLogin;
	private Label emailLoginMessage;

	private LineEdit passwordLogin;
	private Label passwordLoginMessage;

	private CheckBox checkBoxLogin;
	private Button loginButton;

	private LineEdit usernameCreate;
	private Label usernameMessage;

	private LineEdit emailCreate;
	private Label emailMessage;

	private LineEdit passwordCreate;
	private Label passwordMessage;


	private CheckBox checkBoxCreate;
	private Button createAccountButton;

	private const string CREDENTIALS_FILENAME = "user://credentials.json.enc";

	public override void _Ready()
	{
		SetupLoginTab();
		SetupCreateAccountTab();
		CheckSavedPassword();
		Authenticator.OnAuthError += HandleAuthError;
	}

	private void SetupLoginTab() {
		var parent = GetNode("TabContainer").GetNode("Login");
		var firstChild = parent.GetNode("VBox");

		emailLogin = firstChild.GetNode<LineEdit>("EmailInput");
		emailLoginMessage = firstChild.GetNode<Label>("EmailMessageLabel");
		emailLogin.Connect("text_changed", this, nameof(EmailChangedLogin));

		passwordLogin = firstChild.GetNode<LineEdit>("PasswordInput");
		passwordLoginMessage = firstChild.GetNode<Label>("LoginMessageLabel");
		passwordLogin.Connect("text_changed", this, nameof(PasswordChangedLogin));

		checkBoxLogin = firstChild.GetNode<CheckBox>("CheckBox");

		loginButton = parent.GetNode<Button>("Button");
		loginButton.Connect("pressed", this, nameof(LoginButtionPressed));
	}


	private void EmailChangedLogin(string newText) {
		LineEditChanged(newText, emailLogin, emailLoginMessage, Validator.ValidateEmail);
	}

	private void PasswordChangedLogin(string newText) {
		LineEditChanged(newText, passwordLogin, passwordLoginMessage, Validator.ValidatePassword);
	}

	private void CheckSavedPassword() {
		var file = new File();
		if(!file.FileExists(CREDENTIALS_FILENAME))
			return;
		string encryptionPassword = GetEncryptionPassword();
		if(file.OpenEncryptedWithPass(CREDENTIALS_FILENAME, File.ModeFlags.Read, encryptionPassword) == Error.Ok)
			LoadCreadentials(file);
	}

	private void LoadCreadentials(File file) {
		var result = JSON.Parse(file.GetAsText()).Result;
		if(result.GetType() == typeof(Godot.Collections.Dictionary)) {
			var resultDict = (Godot.Collections.Dictionary) result;
			SetCredentials((string) resultDict["email"],(string) resultDict["password"]);
		}
	}

	private string GetEncryptionPassword() {
		return (string) GetNode("/root/Build").Get("encryption_password");
	}

	private void SetCredentials(string email, string password) {
		this.emailLogin.Text = email;
		this.passwordLogin.Text = password;
	}

	private void SaveCredentials(String email, string password) {
		var file = new File();
		file.OpenEncryptedWithPass(CREDENTIALS_FILENAME, File.ModeFlags.Write, GetEncryptionPassword());
		var credentials = new Godot.Collections.Dictionary();
		credentials.Add("email", email);
		credentials.Add("password", password);
		file.StoreLine(JSON.Print(credentials));
		file.Close();
	}

	private void SetupCreateAccountTab() {
		var parent = GetNode("TabContainer/CreateAccount");
		var firstChild = parent.GetNode("VBox");

		usernameCreate = firstChild.GetNode<LineEdit>("UsernameInput");
		usernameCreate.Connect("text_changed", this, nameof(UsernameChanged));
		usernameMessage = firstChild.GetNode<Label>("UsernameMessageLabel");

		emailCreate = firstChild.GetNode<LineEdit>("EmailInput");
		emailCreate.Connect("text_changed", this, nameof(EmailChanged));
		emailMessage = firstChild.GetNode<Label>("EmailMessageLabel");

		passwordCreate = firstChild.GetNode<LineEdit>("PasswordInput");
		passwordCreate.Connect("text_changed", this, nameof(PasswordChanged));
		passwordMessage = firstChild.GetNode<Label>("PasswordMessageLabel");

		createAccountButton = parent.GetNode<Button>("Button");
		createAccountButton.Connect("pressed", this, nameof(CreateAccountButtonPressed));

		checkBoxCreate = firstChild.GetNode<CheckBox>("CheckBox");
	}

	private static void LineEditChanged(string newText, LineEdit lineEdit, Label message, Func<string,VALIDATION_MESSAGE> validator) {
		if(newText.Length == 0) {
			lineEdit.ThemeTypeVariation = "";
			message.Visible = false;
			return;
		}
		SetLineEditMessage(newText, lineEdit, message, validator);
	}

	private static void SetLineEditMessage(string newText, LineEdit lineEdit, Label message, Func<string,VALIDATION_MESSAGE> validator) {
		var mesg = validator(newText);
		if(mesg == VALIDATION_MESSAGE.SUCCESFULL) {
			lineEdit.ThemeTypeVariation = LINE_EDIT_VALID;
			message.Visible = false;
		} else {
			lineEdit.ThemeTypeVariation = LINE_EDIT_ERROR;
			message.Visible = true;
			message.Text = Validator.CreateMessage(mesg);
		}
	}

	private void EmailChanged(string newText) {
		LineEditChanged(newText, emailCreate, emailMessage, Validator.ValidateEmail);
	}

	private void UsernameChanged(string newText) {
		LineEditChanged(newText, usernameCreate, usernameMessage, Validator.ValidateUsername);
	}

	private void PasswordChanged(String newText) {
		LineEditChanged(newText, passwordCreate, passwordMessage, Validator.ValidatePassword);
	}

	private void LoginButtionPressed() {
		SetLineEditMessage(emailLogin.Text, emailLogin, emailLoginMessage, Validator.ValidateEmail);
		SetLineEditMessage(passwordLogin.Text, passwordLogin, passwordLoginMessage, Validator.ValidatePassword);
		if(emailLogin.ThemeTypeVariation == LINE_EDIT_VALID && passwordLogin.ThemeTypeVariation == LINE_EDIT_VALID) {
			this.Visible = false;
			LoginEmail();
		}
	}
	
	private async Task LoginEmail() {
		await Authenticator.AuthenticateEmailAndCreate(emailLogin.Text, passwordLogin.Text, null, false, GetNode("/root/Authenticator"));
		if(checkBoxLogin.Pressed)
			SaveCredentials(emailLogin.Text, passwordLogin.Text);
		OnLoginSuccesfull();
	}

	private void CreateAccountButtonPressed() {
		SetLineEditMessage(emailCreate.Text, emailCreate, emailMessage, Validator.ValidateEmail);
		SetLineEditMessage(passwordCreate.Text, passwordCreate, passwordMessage, Validator.ValidatePassword);
		SetLineEditMessage(usernameCreate.Text, usernameCreate, usernameMessage, Validator.ValidateUsername);
		if(emailCreate.ThemeTypeVariation == LINE_EDIT_VALID && usernameCreate.ThemeTypeVariation == LINE_EDIT_VALID
			&& passwordCreate.ThemeTypeVariation == LINE_EDIT_VALID) {
				this.Visible = false;
				CreateAccount();
		}
	}

	private async Task CreateAccount() {
		await Authenticator.AuthenticateEmailAndCreate(emailCreate.Text, passwordCreate.Text, usernameCreate.Text, true, GetNode("/root/Authenticator"));
		if(checkBoxCreate.Pressed)
			SaveCredentials(emailCreate.Text, passwordCreate.Text);
		OnLoginSuccesfull();
		this.Visible = false;
	}


	private void HandleAuthError(ApiResponseException error) {
		switch(error.GrpcStatusCode) {
			case 5:
				this.Visible = true;
				passwordLoginMessage.Visible = true;
				passwordLoginMessage.Text = "Wrong e-mail or password";
				emailLogin.ThemeTypeVariation = "";
				passwordLogin.ThemeTypeVariation = "";
				break;
		}
	}
	
}
}
