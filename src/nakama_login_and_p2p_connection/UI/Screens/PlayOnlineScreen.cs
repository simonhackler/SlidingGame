using Godot;
using System;
using NakamaWebrtcP2P.Logic;

namespace NakamaWebrtcP2P.UI {
public class PlayOnlineScreen : Screen
{

	public event Action OnMatchmakerButtonPressed = delegate { };
	public event Action OnCreateMatchButtonPressed = delegate { };
	public event Action OnJoinMatchButtonPressed = delegate { };

	private LineEdit matchCode;

	public override void _Ready() {
		var parent = GetNode("PanelContainer").GetNode("VBoxContainer");
		parent.GetNode<Button>("MatchmakerButton").Connect("pressed", this, nameof(MatchmakerButtonPressed));
		parent.GetNode<Button>("CreateMatchButton").Connect("pressed", this, nameof(CreateMatchButtonPressed));

		parent.GetNode("HBoxContainer").GetNode<Button>("JoinMatchButton").Connect("pressed", this, nameof(JoinMatchButtonPressed));
		matchCode = parent.GetNode("HBoxContainer").GetNode<LineEdit>("MatchCodeInput");
	}

	public override void Activate() {
		this.Visible = true;
		GetNode("PanelContainer").GetNode("VBoxContainer").GetNode("HBoxContainer").GetNode<LineEdit>("MatchCodeInput").Text = "";
	}

	private void MatchmakerButtonPressed() {
		OnMatchmakerButtonPressed();
		Authenticator.OnlineMatch.StartMatchmaking(Authenticator.NakamaSocket);
	}

	private void CreateMatchButtonPressed() {
		OnCreateMatchButtonPressed();
		Authenticator.OnlineMatch.CreateMatch(Authenticator.NakamaSocket);
	}

	private void JoinMatchButtonPressed() {
		OnJoinMatchButtonPressed();
		Authenticator.OnlineMatch.JoinMatch(Authenticator.NakamaSocket, matchCode.Text);
	}


}
}
