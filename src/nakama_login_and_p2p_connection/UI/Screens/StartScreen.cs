using Godot;
using System;

namespace NakamaWebrtcP2P.UI {
public class StartScreen : Screen
{

	public event Action OnPlayOnlineButtonPressed = delegate {};
	public event Action OnPlayLocalButtonPressed = delegate {};
	public event Action OnTutorialButtonPressed = delegate {};

	public override void _Ready()
	{
		var parent = GetNode("PanelContainer").GetNode("VBoxContainer");
		parent.GetNode<Button>("PlayOnlineButton")
			.Connect("pressed", this, nameof(PlayOnlineButtonPressed));
		if(OS.GetName() == "HTML5") {
			parent.GetNode<Button>("PlayOnlineButton").QueueFree();
			parent.GetNode<Control>("RichTextLabel").Visible = true;
		} else {
			parent.GetNode<Control>("RichTextLabel").QueueFree();
			parent.GetNode<Button>("PlayOnlineButton").Visible = true;
		}

		parent.GetNode<Button>("PlayLocalButton")
			.Connect("pressed", this, nameof(PlayLocalButtonPressed));
		parent.GetNode<Button>("TutorialButton")
			.Connect("pressed", this, nameof(TutorialButtonPressed));
	}

	private void PlayOnlineButtonPressed() {
		OnPlayOnlineButtonPressed();
	}

	private void PlayLocalButtonPressed() {
		OnPlayLocalButtonPressed();
	}

	private void TutorialButtonPressed() {
		OnTutorialButtonPressed();
	}
}
}
