using Godot;

public abstract class Screen : Control {

	public virtual void Activate() {
		this.Visible = true;
	}
	public virtual void Deactivate() {
		this.Visible = false;
	}

}

