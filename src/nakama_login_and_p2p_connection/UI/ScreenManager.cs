using Godot;
using Nakama;
using NakamaWebrtcP2P.Logic;
using System;
using System.Collections.Generic;

namespace NakamaWebrtcP2P.UI {
public class ScreenManager : CanvasLayer
{

	public event Action OnPlayLocalButtonPressed = delegate { };
	public event Action OnTutorialButtonPressed = delegate { };

	private Stack<Screen> runThroughMenus = new Stack<Screen>();

	private Button backButton;

	public override void _Ready() {
		var startScreen = GetNode("Screens").GetNode<StartScreen>("StartScreen");	
		var connectionScreen = GetNode("Screens").GetNode<ConnectionScreen>("ConnectionScreen");	
		var playOnlineScreen = GetNode("Screens").GetNode<PlayOnlineScreen>("PlayOnlineScreen");
		var matchScreen = GetNode("Screens").GetNode<MatchScreen>("MatchScreen");
		var matchmakingScreen = GetNode("Screens").GetNode<MatchmakingScreen>("MatchmakingScreen");

		backButton = GetNode<Button>("BackButton");
		backButton.Connect("pressed", this, nameof(BackButtonPressed));

		OnlineMatch.OnMatchReady += (x) => OnMatchStart();

		SetupStartScreen(startScreen, connectionScreen);

		connectionScreen.OnLoginSuccesfull += () => ActivateScreen(playOnlineScreen);

		playOnlineScreen.OnMatchmakerButtonPressed += () => ActivateScreen(matchmakingScreen);
		playOnlineScreen.OnCreateMatchButtonPressed += () => ActivateScreen(matchScreen);

		runThroughMenus.Push(startScreen);
	}

	public void EnableAfterMatch() {
		if(runThroughMenus.Peek().GetType() == typeof(MatchScreen) || runThroughMenus.Peek().GetType() == typeof(MatchmakingScreen))
			runThroughMenus.Pop();
		runThroughMenus.Peek().Activate();	
		backButton.Visible = true;
		GetNode<Control>("Screens/Panel").Visible = true;
	}

	private void SetupStartScreen(StartScreen startScreen, ConnectionScreen connectionScreen) {
		startScreen.OnPlayOnlineButtonPressed += () => ActivateScreen(connectionScreen);
		startScreen.OnPlayLocalButtonPressed += PlayLocalButtonPressed;
		startScreen.OnTutorialButtonPressed += TutorialButtonPressed;
	}

	private void ActivateScreen(Screen menu) {
		runThroughMenus.Peek().Deactivate();
		menu.Activate();
		runThroughMenus.Push(menu);
	}

	private void PlayLocalButtonPressed() {
		OnPlayLocalButtonPressed();
		OnMatchStart();
	}

	private void TutorialButtonPressed() {
		OnTutorialButtonPressed();
		OnMatchStart();
	}

	private void OnMatchStart() {
		runThroughMenus.Peek().Visible = false;
		backButton.Visible = false;
		GetNode<Control>("Screens/Panel").Visible = false;
	}

	private void BackButtonPressed() {
		if(runThroughMenus.Count == 1)
			return;
		runThroughMenus.Pop().Deactivate();
		runThroughMenus.Peek().Activate();
	}

}
}
