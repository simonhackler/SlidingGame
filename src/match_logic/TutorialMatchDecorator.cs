
using System;
using System.Collections.Generic;

namespace SlidingGame.Logic {
	public class TutorialMatchDecorator : IMatch, IMatchInteractable
	{

		private Match match;
		public bool IsInStonePlacing => match.IsInStonePlacing;

		public event Action<int, int, int> OnStonePlaced;
		public event Action<int, int, int, int> OnArrowPlaced;
		public event Action<int, int, List<(int, int)>, int> OnStoneMove;
	 	public event Action<int, int, int, int> OnArrowRotated;
		public event Action OnPhaseSwitch;
		public event Action<int> OnGameOver = delegate { };
		public event Action<int, int> OnStoneDestroyed;
		public event Action<int> OnStoneInMiddle;


		private Func<(int, int)> ValidClickPeek;
		private Func<(int, int, int)> ValidClickArrowPeek;
		private Func<(int, int, int)> ValidRotationPeek;
		private Func<(int, int, int, int)> ValidStoneMoves;

		public TutorialMatchDecorator(Match match, Func<(int, int)> ValidClickPeek, 
				Func<(int, int, int)> ValidClickArrowPeek, Func<(int, int, int, int)> ValidStoneMoves, Func<(int, int, int)> ValidRotationPeek) {
			this.match = match;
			this.ValidClickPeek = ValidClickPeek;
			this.ValidClickArrowPeek = ValidClickArrowPeek;
			this.ValidStoneMoves = ValidStoneMoves;
			this.ValidRotationPeek = ValidRotationPeek;

			match.OnStonePlaced += (x,y,z) => OnStonePlaced(x,y,z);
			match.OnArrowPlaced += (x,y,z,w) => OnArrowPlaced(x,y,z,w);
			match.OnStoneMove += (x,y,z,w) => OnStoneMove(x,y,z,w);
			match.OnArrowRotated += (x,y,z,w) => OnArrowRotated(x,y,z,w);
			match.OnStoneDestroyed += (x,y) => OnStoneDestroyed(x,y);
			match.OnStoneInMiddle += (x) => OnStoneInMiddle(x);
			match.OnGameOver += (x) => OnGameOver(x);
		}

		public bool CanMoveStoneTo(int start_x, int start_y, int target_x, int target_y, int playerId) {
			if(playerId == 1) {
				var peek = ValidStoneMoves();
				if(peek.Item1 != start_x || peek.Item2 != start_y || peek.Item3 != target_x || peek.Item4 != target_y) {
					return false;
				}
			}
			return match.CanMoveStoneTo(start_x, start_y, target_x, target_y, playerId);
		}

		public bool CanPlaceArrow(int x, int y, int playerId) {
			return CanPlace(x,y,playerId);
		}

		public bool CanPlace(int x, int y, int playerId) {
			if(!match.CanPlace(x,y,playerId))
				return false;
			if(playerId == 1 && IsInStonePlacing) {
				var click = ValidClickPeek();
				if(click.Item1 != x || click.Item2 != y)
					return false;
				else
					return true;
			} else if(playerId == 1) {
				var click = ValidClickArrowPeek();
				if(click.Item1 != x || click.Item2 != y)
					return false;
				else
					return true;
			}
			return true;
		}

		public bool CanRotate(int x, int y, int playerId) {
			return match.CanRotate(x,y,playerId);
		}

		public void Concede(int concedingPlayer) {
			match.Concede(concedingPlayer);
		}

		public int CurrentPlayer() {
			return match.CurrentPlayer();
		}

		public MapObject GetMapObj(int x, int y) {
			return match.GetMapObj(x,y);
		}

		public bool InMovePhase() {
			return match.InMovePhase();
		}

		public void MoveStone(int start_x, int start_y, int target_x, int target_y, int playerId) {
			match.MoveStone(start_x, start_y, target_x, target_y, playerId);
		}

		public void PlaceArrow(int x, int y, int rotation, int playerId) {
			match.PlaceArrow(x,y,rotation,playerId);
		}

		public void PlaceStone(int x, int y, int playerId) {
			match.PlaceStone(x,y,playerId);
		}

		public void RotateArrow(int x, int y, int rotation, int playerId) {
			match.RotateArrow(x,y,rotation,playerId);
		}

		public bool RotationIsValid(int x, int y, int rotation) {
			if(CurrentPlayer() == 1 && !InMovePhase()) {
				var click = ValidClickArrowPeek();
				if(rotation != click.Item3) {
					return false;
				}
			} else if(CurrentPlayer() == 1 && InMovePhase()) {
				var click = ValidRotationPeek();
				if(rotation != click.Item3 || x != click.Item1 || y != click.Item2) {
					return false;
				}
			}
			return match.RotationIsValid(x,y,rotation);
		}
	}
}
