using System;
using System.Collections.Generic;

namespace SlidingGame.Logic {


public interface IMatchInteractable {

	event Action<int, int, int> OnStonePlaced;
	event Action<int, int, int, int> OnArrowPlaced;
	event Action<int, int, List<(int,int)>, int> OnStoneMove;
	event Action<int, int, int, int> OnArrowRotated;
	event Action OnPhaseSwitch;
	event Action<int> OnGameOver;
	event Action<int, int> OnStoneDestroyed;
	event Action<int> OnStoneInMiddle;

	bool InMovePhase();
	MapObject GetMapObj(int x, int y);
	int CurrentPlayer();
	bool CanPlaceArrow(int x, int y, int playerId);
	bool CanPlace(int x, int y, int playerId);
	bool CanMoveStoneTo(int start_x, int start_y, int target_x, int target_y, int playerId); 
	bool CanRotate(int x, int y, int playerId); 
	bool RotationIsValid(int x, int y, int rotation); 
	bool IsInStonePlacing{get;}
}

}
