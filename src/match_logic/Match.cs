using System;
using System.Collections.Generic;

namespace SlidingGame.Logic {

public class Match : IMatchInteractable, IMatch {

	public event Action<int, int, int> OnStonePlaced = delegate { };
	public event Action<int, int, int, int> OnArrowPlaced = delegate { };

	public event Action<int, int, List<(int,int)>, int> OnStoneMove = delegate { };
	public event Action<int, int, int, int> OnArrowRotated = delegate { };
	public event Action OnPhaseSwitch = delegate { };

	public event Action<int> OnGameOver = delegate { };

	public event Action<int, int> OnStoneDestroyed = delegate { };
	public event Action<int> OnStoneInMiddle = delegate { };

	public bool IsInStonePlacing {get; private set;}

	private bool gameOver;

	private MapObject[,] mapObjects;

	private int player1;
	private int player2;

	private bool player1Turn;

	private int maxPlace;
	private int currentPlace;
	private int maxRotate;
	private int currentRotate;
	private int size;

	private int player1StonesAmount;
	private int player2StonesAmount;
	private int player1StonesSave;
	private int player2StonesSave;

	private (int, int) lastRotated = (-1, -1);

	public Match(int size, int stonesAmount,int _player1, int _player2) {
		gameOver = false;
		this.size = size;
		mapObjects = new MapObject[size,size];

		player1 = _player1;
		player2 = _player2;
		player1Turn = true;
		IsInStonePlacing = true;

		maxPlace = stonesAmount * 4;
		currentPlace = 0;

		maxRotate = 1;
		currentRotate = 0;

		player1StonesAmount = stonesAmount;
		player2StonesAmount = stonesAmount;
		player1StonesSave = 0;
		player2StonesSave = 0;
	}

	public void Concede(int concedingPlayer) {
		if(concedingPlayer == player2) {
			OnGameOver(player1);
		} else {
			OnGameOver(player2);
		}
	}

	public bool InMovePhase() {
		return currentPlace >= maxPlace;
	}

	public MapObject GetMapObj(int x, int y) {
		return mapObjects[x,y];
	}

	public int CurrentPlayer() {
		return player1Turn ? player1 : player2;
	}

	public bool CanPlaceArrow(int x, int y, int playerId) {
		if(!CanPlace(x,y,playerId))
			return false;
		if(x == 0 && y == 0) {
			if(!RotationIsValid(x,y, 180) && !RotationIsValid(x,y,-90)) {
				return false;
			}
		} else if(x == 0 && y == size - 1) {
			if(!RotationIsValid(x,y, 180) && !RotationIsValid(x,y,90)) {
				return false;
			}
		} else if(x == size - 1 && y == 0) {
			if(!RotationIsValid(x,y, 0) && !RotationIsValid(x,y,-90)) {
				return false;
			}
		} else if(x == size - 1 && y == size - 1) {
			if(!RotationIsValid(x,y, 0) && !RotationIsValid(x,y,90)) {
				return false;
			}
		}
		return true;
	}

	public bool CanPlace(int x, int y, int playerId) {
		if(currentPlace >= maxPlace) {
			return false;
		} else if(!IsPlayerTurn(playerId)) {
			return false;
		}
		if(mapObjects[x,y] != null) {
			return false; }
		if(IsInMiddle(x,y)) {
			return false;
		}
		return true;
	}

	private bool IsPlayerTurn(int playerId) {
		return CurrentPlayer() == playerId;
	}

	public void PlaceStone(int x, int y, int playerId) {
		if(!CanPlace(x,y,playerId))
			throw new System.Exception("Cant place stone here");
		if(!IsInStonePlacing) {
			throw new System.Exception("Trying to place stone in wrong phase");
		}
		mapObjects[x,y] = new Stone(playerId);
		OnStonePlaced(x,y,playerId);
		SwitchPlacePhaseAndPlayer();
	}

	public bool CanMoveStoneTo(int start_x, int start_y, int target_x, int target_y, int playerId) {
		if(!InMovePhase()) {
			return false;
		}
		if(!IsPlayerTurn(playerId)) {
			return false;
		} 
		if(target_x < 0 || target_y < 0 || target_x >= size || target_y >= size)
			return false;
		double distance = Math.Pow(start_x - target_x, 2) + Math.Pow(start_y - target_y, 2);
		if(distance <= 0 || distance > 1) {
			return false;
		}
		var target = mapObjects[target_x, target_y];
		if(mapObjects[start_x, start_y] == null) {
			return false;
		} else if(mapObjects[start_x, start_y].GetType() != typeof(Stone)) {
			return false;
		} else if(mapObjects[start_x, start_y].PlayerId != playerId) {
			return false;
		} else if(mapObjects[target_x, target_y] != null && target.GetType() == typeof(Stone)) {
			return false;
		} 
		return true;
	}

	public void MoveStone(int start_x, int start_y, int target_x, int target_y, int playerId) {
		if(!CanMoveStoneTo(start_x, start_y,target_x, target_y, playerId)) {
			throw new System.Exception("Cant move this stone bruh");
		}
		var target = mapObjects[target_x, target_y];
		List<(int,int)> locations = new List<(int, int)>();
		locations.Add((target_x, target_y));
		var stoneToMove = mapObjects[start_x, start_y];
		mapObjects[start_x, start_y] = null;
		if(target != null && target.GetType() == typeof(Arrow)) {
			var arrow = (Arrow) target;
			CalculateMovement(arrow, target_x, target_y, locations);
			target_x = locations[locations.Count - 1].Item1;
			target_y = locations[locations.Count - 1].Item2;
		} 
		OnStoneMove(start_x, start_y, locations, playerId);
		if(IsInMiddle(target_x, target_y)) {
			OnStoneInMiddle(CurrentPlayer());	
			if(playerId == player1) {
				player1StonesSave += 1;
				player1StonesAmount -= 1;
			}
			else {
				player2StonesSave += 1;
				player2StonesAmount -= 1;
			}
			SetGameOver();
		} else {
			mapObjects[target_x, target_y] = stoneToMove;
		}

		if(gameOver)
			GameOver();
		
		player1Turn = !player1Turn;
		currentRotate = 0;
	}

	private void GameOver() {
		player1StonesSave += player1StonesAmount;
		player2StonesSave += player2StonesAmount;
		if(player1StonesSave > player2StonesSave) {
			OnGameOver(player1);
		} else if(player1StonesSave < player2StonesSave) {
			OnGameOver(player2);
		} else {
			OnGameOver(-1);
		}
	}

	private (int, int) GetDirection(int rotation) {
		switch(rotation) {
			case 0:
				return (-1,0);
			case 180:
				return (1,0);
			case 90:
				return (0, -1);
			case -90:
				return (0, 1);
			default:
				throw new System.Exception("This rotation does not exist");
		}
	}

	private (int, int) CalculateMovement(Arrow arrow, int arrow_x, int arrow_y, List<(int,int)> locations) {
		var direction = GetDirection(arrow.Rotation);
		int xCounter = arrow_x;
		int yCounter = arrow_y;
		xCounter += direction.Item1;
		yCounter += direction.Item2;
		while(xCounter >= 0 && xCounter <= size - 1 && yCounter >= 0 && yCounter <= size - 1) {
			if(IsInMiddle(xCounter, yCounter)){
				locations.Add(Middle());
				return Middle();
			}
			else if(mapObjects[xCounter,yCounter] != null) {
				if(mapObjects[xCounter, yCounter].GetType() == typeof(Arrow)) {
					locations.Add((xCounter, yCounter));
					(int, int) pos = CalculateMovement((Arrow) mapObjects[xCounter, yCounter], xCounter, yCounter, locations);
					return pos;
				}
				else if(mapObjects[xCounter, yCounter].GetType() == typeof(Stone)) {
					Destroy(xCounter, yCounter);
				}
			}
			xCounter += direction.Item1;
			yCounter += direction.Item2;
		}
		xCounter -= direction.Item1;
		yCounter -= direction.Item2;
		locations.Add((xCounter, yCounter));
		return(xCounter , yCounter);
	}

	private void Destroy(int x, int y) {
		if(mapObjects[x,y].PlayerId == player1)
			player1StonesAmount--;
		else
			player2StonesAmount--;
		mapObjects[x,y] = null;
		OnStoneDestroyed(x,y);
		SetGameOver();
	}

	private void SetGameOver() {
		gameOver = player1StonesAmount == 0 || player2StonesAmount == 0;
	}

	private void SwitchPlacePhaseAndPlayer() {
		if(!player1Turn)
			IsInStonePlacing = !IsInStonePlacing;
		player1Turn = !player1Turn;
		currentPlace += 1;
		if(currentPlace >= maxPlace) {
			OnPhaseSwitch();
		}
	}

	public void PlaceArrow(int x, int y, int rotation, int playerId) {
		if(!CanPlace(x,y,playerId))
			throw new System.Exception("Cant place arrow there");
		if(IsInStonePlacing) 
			throw new System.Exception("Trying to place arrow in wrong phase");
		if(!RotationIsValid(x,y,rotation))
			throw new System.Exception("Rotation is no good");
		mapObjects[x,y] = new Arrow(playerId, rotation);
		OnArrowPlaced(x,y,rotation,playerId);
		SwitchPlacePhaseAndPlayer();
	}

	public bool CanRotate(int x, int y, int playerId) {
		if(!InMovePhase())
			return false;
		else if(currentRotate >= maxRotate)
			return false;
		else if(!IsPlayerTurn(playerId))
			return false;
		else if(mapObjects[x,y] == null || mapObjects[x,y].GetType() != typeof(Arrow))
			return false;
		else if(lastRotated.Item1 == x && lastRotated.Item2 == y)
			return false;
		return true;
	}

	public void RotateArrow(int x, int y, int rotation, int playerId) {
		if(!CanRotate(x,y,playerId))
			throw new System.Exception("Boi dont try to rotate you know it aint right");
		if(!RotationIsValid(x,y,rotation))
			throw new System.Exception("Nah fam this aint a valid rotation");
		((Arrow) mapObjects[x,y]).Rotation = rotation;
		OnArrowRotated(x,y,rotation,playerId);
		currentRotate += 1;
		lastRotated = (x,y);
	}

	private (int, int)? GetNextArrowPosOrNull(int x, int y, int rotation, int x_start, int y_start) {
		var direction = GetDirection(rotation);
		int xCounter = x;
		int yCounter = y;
		xCounter += direction.Item1;
		yCounter += direction.Item2;
		if(xCounter < 0 || xCounter >= size || yCounter < 0 || yCounter >= size)
			return null;
		while(xCounter >= 0 && xCounter <= size - 1 && yCounter >= 0 && yCounter <= size - 1) {
			if(xCounter == x_start && yCounter == y_start)
				return null;
			if(IsInMiddle(xCounter, yCounter)){
				return (xCounter, yCounter);
			}
			else if(mapObjects[xCounter,yCounter] != null) {
				if(mapObjects[xCounter, yCounter].GetType() == typeof(Arrow)) {
					return ( xCounter, yCounter); 
				}
			}
			xCounter += direction.Item1;
			yCounter += direction.Item2;
		}
		xCounter -= direction.Item1;
		yCounter -= direction.Item2;
		return (xCounter, yCounter);
	}

	public bool RotationIsValid(int x, int y, int rotation) {
		(int, int)? nextArrowPos = GetNextArrowPosOrNull(x,y,rotation,x,y);
		List<(int, int)?> visited = new List<(int, int)?>();
		do {
			if(visited.Contains(nextArrowPos))
				return false;
			else if(nextArrowPos == null)
				return false;
			var mapObj = mapObjects[nextArrowPos.Value.Item1, nextArrowPos.Value.Item2];
			if(mapObj == null || mapObj.GetType() != typeof(Arrow))
				return true;
			visited.Add(nextArrowPos);
			nextArrowPos = GetNextArrowPosOrNull(nextArrowPos.Value.Item1, nextArrowPos.Value.Item2, ((Arrow) mapObj).Rotation, x, y);	 
		} while(nextArrowPos != null);
		return false;
	}

	private bool IsInMiddle(int x, int y) {
		return (x == mapObjects.GetLength(1) / 2 && y == mapObjects.GetLength(1) / 2);
	}

	private (int, int) Middle() {
		return (mapObjects.GetLength(1) / 2,  mapObjects.GetLength(1)/2);
	}

}
}
