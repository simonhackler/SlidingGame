namespace SlidingGame.Logic{
public class Arrow : MapObject {

	public int Rotation {get; set;}
	public Arrow(int playerId, int rotation) {
		PlayerId = playerId;	
		Rotation = rotation;
	}

}
}
