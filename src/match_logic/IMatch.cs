namespace SlidingGame.Logic {
public interface IMatch {

	void Concede(int concedingPlayer);
	void PlaceStone(int x, int y, int playerId); 
	void MoveStone(int start_x, int start_y, int target_x, int target_y, int playerId); 
	void PlaceArrow(int x, int y, int rotation, int playerId);
	void RotateArrow(int x, int y, int rotation, int playerId);

}
}
