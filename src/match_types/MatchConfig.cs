public struct MatchConfig {

	public readonly int size;
	public readonly int stonesAmount; 
	public readonly int player1;
	public readonly int player2;
	public readonly string playerUsername;
	public readonly string opponentUsername;

    public MatchConfig(int size, int stonesAmount, int player1, int player2, string playerUsername, string opponentUsername)
    {
        this.size = size;
        this.stonesAmount = stonesAmount;
        this.player1 = player1;
        this.player2 = player2;
        this.playerUsername = playerUsername;
        this.opponentUsername = opponentUsername;
    }

	public static MatchConfig SwitchPlayers(MatchConfig config) {
		return new MatchConfig(config.size, config.stonesAmount, config.player2, config.player1, config.playerUsername, config.opponentUsername);
	}

}
