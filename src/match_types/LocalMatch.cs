using System;
using SlidingGame.Logic;

public class LocalMatch : MatchTemplate {

	public override event Action OnLeave = delegate { };
	public override event Action OnRematch = delegate { };

	private int oldPlayerId = 2;
	private int currentPlayerId;

	public LocalMatch(int currentPlayerId) {
	}

	private void SwitchPlayerID() {
		if(currentPlayerId == 1)
			currentPlayerId = 2;
		else
			currentPlayerId = 1;
		gameScene.ChangePlayerID(currentPlayerId);
	}

	protected override void AfterMatchStart() {
		currentPlayerId = oldPlayerId == 2 ? 1 : 2;
		oldPlayerId = currentPlayerId;
		gameScene.ChangePlayerID(currentPlayerId);
	}

	protected override void BeforeMatchStart() {

	}

	protected override void SendTileClick(int x, int y) {
		match.PlaceStone(x,y,currentPlayerId);
		SwitchPlayerID();
	}

	protected override void SendArrowPlaced(int x, int y, int rotation) {
		match.PlaceArrow(x,y,rotation, currentPlayerId);
		SwitchPlayerID();
	}

	protected override void SendStoneMove(int x_start, int y_start, int x_goal, int y_goal) {
		match.MoveStone(x_start, y_start, x_goal, y_goal, currentPlayerId);
		SwitchPlayerID();
	}

	protected override void SendRotation(int x, int y, int rotation) {
		match.RotateArrow(x,y,rotation, currentPlayerId);
	}

	protected override void AskForRematch() {
		OnRematch();
	}

	protected override void Concede() {
		match.Concede(currentPlayerId);
	}

	protected override void Quit() {
		OnLeave();
	}
	

}
