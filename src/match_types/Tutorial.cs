using System;
using System.Collections.Generic;
using SlidingGame.Logic;

public class Tutorial : MatchTemplate {

	public override event Action OnLeave = delegate { };
	public override event Action OnRematch = delegate { };

	private IMatchInteractable matchInt;
	private int myPlayerID;
	private Queue<(int, int)> validClicks = new Queue<(int, int)>( new[] {
		(0,0), (1,4)
	});
	private Queue<(int, int)> opponentClicks = new Queue<(int, int)>( new[] {
		(0,2), (4,4)
	});
	private Queue<(int, int, int)> validClicksRotation = new Queue<(int, int, int)>( new[] {
		(0,1, -90), 
		(0,3, 180), 
	});
	private Queue<(int, int, int)> opponentClicksRotation = new Queue<(int, int, int)>( new[] {
		(3,2, -90), 
		(1,3, -90), 
	});
	private Queue<(int, int, int, int)> validStoneMoves = new Queue<(int, int, int, int)>( new[] {
		(0,0, 0, 1), 
		(1,4, 2, 4), 
		(2,4, 2, 3), 
		(2,3, 2, 2), 
		(1,0, 1, 1), 
		(1,1, 2, 1), 
		(2,1, 2, 2), 
		(2,2, 2, 2), 
	});
	private Queue<(int, int, int, int)> opponentValidStoneMoves = new Queue<(int, int, int, int)>( new[] {
		(4, 4, 4, 3), 
		(4, 3, 3, 3), 
		(3, 3, 3, 4), 
		(3, 4, 2, 4), 
		(2, 4, 1, 4), 
		(1, 4, 1, 3), 
		(1, 0, 1, 1), 
	});
	private Queue<(int, int, int)> validRotations = new Queue<(int, int, int)>( new[] {
		(1,3, 90)
	});
	

	public Tutorial() {
		myPlayerID = 1;
	}


	protected override (IMatchInteractable, IMatch) DecorateMatch(Match _match) {
		var match = new TutorialMatchDecorator(
				_match, validClicks.Peek, PeekClicksRotations, validStoneMoves.Peek, PeekValidRotations);
		matchInt = match;
		return (match, match);
	}

	private (int, int, int) PeekClicksRotations() {
		if(validClicksRotation.Count <= 0)
			return (-1,-1,-1);
		else 
			return validClicksRotation.Peek();
	}

	private (int, int, int) PeekValidRotations() {
		if(validRotations.Count <= 0)
			return (-1,-1,-1);
		else 
			return validRotations.Peek();
	}

	private void HighlighNext() {
		if(matchInt.InMovePhase()) {
			if(validRotations.Count > 0 && matchInt.CanRotate(validRotations.Peek().Item1, validRotations.Peek().Item2, myPlayerID)) {
				this.gameScene.TutorialHighlight(validRotations.Peek().Item1, validRotations.Peek().Item2);
			} else {
				this.gameScene.TutorialHighlight(validStoneMoves.Peek().Item1, validStoneMoves.Peek().Item2);
			}
		} else {
			if(validClicksRotation.Count > 0 && validClicks.Count < validClicksRotation.Count) {
				this.gameScene.TutorialHighlight(validClicksRotation.Peek().Item1, validClicksRotation.Peek().Item2);
			} else if(validClicks.Count > 0) {
				this.gameScene.TutorialHighlight(validClicks.Peek().Item1, validClicks.Peek().Item2);
			}
		}
	}



	protected override void BeforeMatchStart() {
		 validClicks = new Queue<(int, int)>( new[] {
			(0,0), (1,4)
		});
		 opponentClicks = new Queue<(int, int)>( new[] {
			(0,2), (4,4)
		});
		 validClicksRotation = new Queue<(int, int, int)>( new[] {
			(0,1, -90), 
			(0,3, 180), 
		});
		 opponentClicksRotation = new Queue<(int, int, int)>( new[] {
			(3,2, -90), 
			(1,3, -90), 
		});
		 validStoneMoves = new Queue<(int, int, int, int)>( new[] {
			(0,0, 0, 1), 
			(1,4, 2, 4), 
			(2,4, 2, 3), 
			(2,3, 2, 2), 
			(1,0, 1, 1), 
			(1,1, 2, 1), 
			(2,1, 2, 2), 
			(2,2, 2, 2), 
		});
		 opponentValidStoneMoves = new Queue<(int, int, int, int)>( new[] {
			(4, 4, 4, 3), 
			(4, 3, 3, 3), 
			(3, 3, 3, 4), 
			(3, 4, 2, 4), 
			(2, 4, 1, 4), 
			(1, 4, 1, 3), 
			(1, 0, 1, 1), 
		});
		 validRotations = new Queue<(int, int, int)>( new[] {
			(1,3, 90)
		});
	}

	protected override void AfterMatchStart() {
		HighlighNext();
		gameScene.ChangePlayerID(1);
	}

	protected override void SendTileClick(int x, int y) {
		if(validClicks.Peek().Item1 != x ||validClicks.Peek().Item2 != y)
			return;
		gameScene.Unhighlight(validClicks.Peek().Item1,validClicks.Peek().Item2);
		validClicks.Dequeue();
		match.PlaceStone(x,y,myPlayerID);
		var click = opponentClicks.Dequeue();
		match.PlaceStone(click.Item1, click.Item2, 2);
		HighlighNext();
	}

	protected override void SendArrowPlaced(int x, int y, int rotation) {
		var top = validClicksRotation.Peek();
		if(top.Item1 != x || top.Item2 != y || top.Item3 != rotation)
			return;
		gameScene.Unhighlight(top.Item1, top.Item2);
		match.PlaceArrow(x,y,rotation, myPlayerID);
		validClicksRotation.Dequeue();

		var click = opponentClicksRotation.Dequeue();
		match.PlaceArrow(click.Item1,click.Item2,click.Item3, 2);
		HighlighNext();
	}

	protected override void SendStoneMove(int x_start, int y_start, int x_goal, int y_goal) {
		if(validRotations.Count > 0 && matchInt.CanRotate(validRotations.Peek().Item1, validRotations.Peek().Item2, myPlayerID))
			return;
		match.MoveStone(x_start, y_start, x_goal, y_goal, myPlayerID);
		var top = validStoneMoves.Dequeue();
		gameScene.Unhighlight(top.Item1, top.Item2);

		var oppMove = opponentValidStoneMoves.Dequeue();
		match.MoveStone(oppMove.Item1, oppMove.Item2, oppMove.Item3, oppMove.Item4, 2);
		HighlighNext();
	}

	protected override void SendRotation(int x, int y, int rotation) {
		match.RotateArrow(x,y,rotation, myPlayerID);
		var top = validRotations.Dequeue();
		gameScene.Unhighlight(top.Item1, top.Item2);
		HighlighNext();
	}

	protected override void AskForRematch() {
		OnRematch();
	}

	protected override void Concede() {
		match.Concede(myPlayerID);
	}

	protected override void Quit() {
		OnLeave();
	}
	

}
