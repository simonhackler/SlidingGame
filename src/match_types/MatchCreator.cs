using Godot;
using System.Collections.Generic;
using NakamaWebrtcP2P.Logic;
using NakamaWebrtcP2P.UI;
using System.Linq;

public class MatchCreator : Node
{

	[Export]	
	private PackedScene gamePrefab;

	private GameScene currentGameScene;
	private MatchConfig currentMatchConfig;
	private MatchTemplate currentMatch;
	private int playerId;
	private ScreenManager screenManager;

	public override void _Ready() {
		OnlineMatch.OnMatchReady += CreateAndStartP2PMatch;
		screenManager = GetNode<ScreenManager>("/root/Main/UILayer");
		screenManager.OnPlayLocalButtonPressed += CreateAndStartLocalMatch;
		screenManager.OnTutorialButtonPressed += CreateAndStartTutorial;
	}

	private void CreateAndStartTutorial() {
		var game = InitGamescene();
		var config = new MatchConfig(5, 2, 1, 2, "You", "Impossibly strong AI");
		var match = new Tutorial();
		StartMatch(match, config, config.player1, game);
	}

	private void CreateAndStartLocalMatch() {
		var game = InitGamescene();
		var config = new MatchConfig(7, 6, 1, 2, "Player 1", "Player 2");
		var match = new LocalMatch(config.player1);
		StartMatch(match, config, config.player1, game);
	}

	private void CreateAndStartP2PMatch(Dictionary<string, P2PPlayer> players) {
		var config = CreateP2PMatchConfig(players);
		int myPeerId = GetTree().GetNetworkUniqueId();
		int opponentPeerId = (myPeerId == config.player1 ? config.player2 : config.player1);
		var match = new P2PConnection(myPeerId, opponentPeerId);
		var game = InitGamescene();
		StartMatch(match, config, myPeerId, game); 
	}

	private void StartMatch(MatchTemplate match, MatchConfig config, int myId, GameScene game) {
		currentMatch = match;
		currentMatch.OnRematch += Rematch;
		currentMatch.OnLeave += Leave;
		currentGameScene = game;
		playerId = myId;

		currentMatchConfig = config;
		currentMatch.Name = "P2PConnection";
		GetNode("/root").AddChild(currentMatch);
		currentMatch.StartMatch(config, currentGameScene, playerId);
	}

	private void Rematch() {
		GetTree().Root.RemoveChild(currentGameScene);
		currentGameScene.QueueFree();
		currentGameScene = InitGamescene();
		if(!(currentMatch.GetType() == typeof(Tutorial))) {
			currentMatchConfig = MatchConfig.SwitchPlayers(currentMatchConfig);
		}
		currentMatch.StartMatch(currentMatchConfig, currentGameScene, playerId);
	}

	private void Leave() {
		GetTree().Root.RemoveChild(currentGameScene);
		currentGameScene.QueueFree();
		screenManager.EnableAfterMatch();
		currentGameScene = null;
		currentMatch.QueueFree();
		currentMatch = null;
		GD.Print("Leaving");
	}

	private MatchConfig CreateP2PMatchConfig(Dictionary<string, P2PPlayer> players) {
		var sessions = players.Keys.ToList();
		sessions.Sort();
		int myPeerId = GetTree().GetNetworkUniqueId();
		string myUsername = null;
		string opponentUsername = null;
		int opponentPeerId = -1;
		foreach(var session in sessions) {
			if(players[session].PeerId != myPeerId) {
				opponentPeerId = players[session].PeerId;
				opponentUsername = players[session].Username;
			} else {
				myUsername = players[session].Username;
			}
		}
		return new MatchConfig(7, 6, players[sessions[0]].PeerId, players[sessions[1]].PeerId, myUsername, opponentUsername);
	}

	private GameScene InitGamescene() {
		var gameScene = gamePrefab.Instance() as GameScene;
		GetNode("/root").AddChild(gameScene);
		return gameScene;
	}


}
