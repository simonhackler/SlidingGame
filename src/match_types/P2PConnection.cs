using Godot;
using System;
using System.Linq;
using SlidingGame.Logic;
using NakamaWebrtcP2P.Logic;

public class P2PConnection : MatchTemplate {

	public override event Action OnRematch = delegate { };
	public override event Action OnLeave = delegate { };

	private int myPeerId;
	private int opponentPeerId;

	private MatchConfig config;

	private bool rematchOfferRecieved;
	private bool rematchOfferSend;

	public override void _ExitTree() {
		Authenticator.OnlineMatch.OnPlayerLeft -= PlayerLeft;
	}

	public P2PConnection(int myPeerId, int opponentPeerId) {
		Authenticator.OnlineMatch.OnPlayerLeft += PlayerLeft;
		this.myPeerId = myPeerId;
		this.opponentPeerId = opponentPeerId;
	}

	protected override void BeforeMatchStart() {
		rematchOfferRecieved = false;
		rematchOfferSend = false;
	}

	private void PlayerLeft(P2PPlayer player) {
		GD.Print("This guy left");
		match.Concede(player.PeerId);
	}

	[Remote]
	private void ReturnStone(int x, int y, int playerId) {
		match.PlaceStone(x, y, playerId);	
	}

	[Remote]
	private void ReturnArrow(int x, int y, int rotation, int playerId) {
		match.PlaceArrow(x,y,rotation,playerId);
	}

	[Remote]
	private void ReturnRotation(int x, int y, int rotation, int playerId) {
		match.RotateArrow(x,y,rotation,playerId);
	}

	[Remote]
	private void ReturnMoveStone(int x, int y, int target_x, int target_y, int playerId) {
		match.MoveStone(x,y,target_x,target_y,playerId);
	}

	[Remote]
	private void RecieveRematchOffer() {
		rematchOfferRecieved = true;
		if(rematchOfferSend) {
			OnRematch();
		}
	}

	protected override void SendArrowPlaced(int x, int y, int rotation) {
		ReturnArrow(x,y,rotation,myPeerId);
		RpcId(opponentPeerId, nameof(ReturnArrow), x, y, rotation, myPeerId);
	}

	protected override void SendTileClick(int x, int y) {
		ReturnStone(x,y,myPeerId);
		RpcId(opponentPeerId, nameof(ReturnStone), x, y, myPeerId);
	}

	protected override void SendStoneMove(int x_start, int y_start, int x_goal, int y_goal) {
		ReturnMoveStone(x_start, y_start, x_goal, y_goal, myPeerId);
		RpcId(opponentPeerId, nameof(ReturnMoveStone), x_start, y_start, x_goal, y_goal, myPeerId);
	}

	protected override void SendRotation(int x, int y, int rotation) {
		ReturnRotation(x,y,rotation,myPeerId);
		RpcId(opponentPeerId, nameof(ReturnRotation), x, y, rotation, myPeerId);
	}

	protected override void AskForRematch() {
		GD.Print("Asking for Rematch");
		RpcId(opponentPeerId, nameof(RecieveRematchOffer));
		rematchOfferSend = true;
		if(rematchOfferRecieved) {
			OnRematch();
		}
	}

	protected override void Concede() {
		GD.Print("Conceding");
		match.Concede(myPeerId);
		RpcId(opponentPeerId, nameof(RecieveConcede));
	}

	[Remote]
	private void RecieveConcede() {
		match.Concede(opponentPeerId);
	}

	protected override void Quit() {
		//if(GetTree().GetNetworkConnectedPeers().Contains(opponentPeerId))
			//Concede();
		Authenticator.OnlineMatch.Leave();
		OnLeave();
	}

}

