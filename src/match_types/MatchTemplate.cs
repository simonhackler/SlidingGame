using Godot;
using System;
using SlidingGame.Logic;


public abstract class MatchTemplate : Node {

	public abstract event Action OnRematch;
	public abstract event Action OnLeave;

	protected IMatch match;
	protected GameScene gameScene;
	
	public void StartMatch(MatchConfig _config, GameScene gameScene, int myPlayerId) {
		BeforeMatchStart();
		var match = new Match(_config.size, _config.stonesAmount, _config.player1, _config.player2);
		this.gameScene = gameScene;
		var myMatch = DecorateMatch(match);

		gameScene.OnConcede += Concede;
		gameScene.OnTileClicked += SendTileClick;
		gameScene.OnRotatePlaced += SendArrowPlaced;
		gameScene.OnMove += SendStoneMove;
		gameScene.OnRotation += SendRotation;
		gameScene.OnRematch += AskForRematch;
		gameScene.OnQuit += Quit;
		gameScene.StartMatch(myMatch.Item1, _config, myPlayerId);
		this.match = myMatch.Item2;
		AfterMatchStart();
	}

	protected virtual (IMatchInteractable, IMatch) DecorateMatch(Match _match) {
		return(_match, _match);
	}

	protected virtual void AfterMatchStart() {

	}

	protected abstract void BeforeMatchStart(); 
	protected abstract void SendArrowPlaced(int x, int y, int rotation);
	protected abstract void SendTileClick(int x, int y); 
	protected abstract void SendStoneMove(int x_start, int y_start, int x_goal, int y_goal); 
	protected abstract void SendRotation(int x, int y, int rotation); 
	protected abstract void AskForRematch(); 
	protected abstract void Concede();
	protected abstract void Quit();
}
