using Godot;

public class PlayerInfo : Panel
{

	private Panel playerActivePanel;
	private RichTextLabel scoreText;
	private int score;

	public override void _Ready() {
		playerActivePanel = GetNode<Panel>("Active");
		scoreText = GetNode("MarginContainer").GetNode("HBoxContainer").GetNode("Panel2").GetNode<RichTextLabel>("Score");
		scoreText.Text = "0";
		score = 0;
	}

	public void SetPlayer(string username) {
		GetNode("MarginContainer").GetNode("HBoxContainer").GetNode("Panel").GetNode<RichTextLabel>("Username").Text = username;
	}

	public void ToggleActive() {
		this.playerActivePanel.Visible = !this.playerActivePanel.Visible;
	}

	public void SetActive() {
		this.playerActivePanel.Visible = true;
	}

	public void SetInactive() {
		this.playerActivePanel.Visible = false;
	}

	public void IncrementScore() {
		score += 1;
		scoreText.Text = score.ToString();
	}

}
