using Godot;
using SlidingGame.Logic;

public class GameHUD : Control
{

	private PlayerInfo playerInfo;
	private PlayerInfo opponentInfo;
	private int myPlayerId;

	public override void _Ready() {
		opponentInfo = GetNode<PlayerInfo>("OpponentInfo");
		playerInfo = GetNode<PlayerInfo>("PlayerInf");
	}

	public void SetPlayerNames(string playerName, string opponentName, IMatchInteractable match, bool playerActive, int myPlayerId) {
		this.myPlayerId = myPlayerId;
		match.OnStoneMove += (x,y,z,w) => TogglePlayerActiveUIMove();
		match.OnArrowPlaced  += (x,y,z,w) => TogglePlayerActiveUIMove();
		match.OnStonePlaced += (x,y,z) => TogglePlayerActiveUIMove();
		match.OnStoneInMiddle += UpdateScores;
		if(playerActive) {
			playerInfo.SetActive();
			opponentInfo.SetInactive();
		} else {
			playerInfo.SetInactive();
			opponentInfo.SetActive();
		}
		playerInfo.SetPlayer(playerName);
		opponentInfo.SetPlayer(opponentName);
	}

	private void UpdateScores(int playerId) {
		if(myPlayerId == playerId)
			playerInfo.IncrementScore();
		else
			opponentInfo.IncrementScore();
	}

	private void TogglePlayerActiveUIMove() {
		playerInfo.ToggleActive();
		opponentInfo.ToggleActive();
	}

}
