using Godot;

public class PlaceArrowTileButton : TextureButton
{

	private Tween tween;

	private float scaleStart = 1;
	private float scaleEnd = 1.1f;
	private float tweenTime = 0.15f;

	private float currentScale;



	public override void _Ready()
	{
	   tween = this.GetParent().GetParent().GetNode<Tween>("Tween"); 
	   currentScale = scaleStart;
	   this.Connect("mouse_entered", this, nameof(ScaleNodeUp)); 
	   this.Connect("mouse_exited", this, nameof(ScaleNodeDown)); 
	   this.RectPivotOffset = this.RectSize / 2;
	}

	public override void _Process(float delta) {
		this.RectScale = new Vector2(currentScale, currentScale);
	}

	private void ScaleNodeUp() {
		if(this.Disabled)
			return;
		if(tween.IsActive())
			tween.Stop(this, "currentScale");
		tween.InterpolateProperty(this, "currentScale", scaleStart, scaleEnd, tweenTime, Tween.TransitionType.Linear,Tween.EaseType.Out);
		tween.Start();
	}

	private void ScaleNodeDown() {
		if(this.Disabled)
			return;
		if(tween.IsActive())
			tween.Stop(this, "currentScale");
		tween.InterpolateProperty(this, "currentScale", scaleEnd, scaleStart, tweenTime, Tween.TransitionType.Linear,Tween.EaseType.Out);
		tween.Start();
	}
	

}
