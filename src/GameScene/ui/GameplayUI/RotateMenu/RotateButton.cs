using Godot;

public class RotateButton : TextureButton
{

	[Export]
	private int endRotation = 90;
	private Tween tween;

	private float tweenTime = 0.15f;
	private int startRotation;

	private int currentRotation;

	public override void _Ready()
	{
	   tween = this.GetParent().GetParent().GetNode<Tween>("Tween"); 
	   this.Connect("mouse_entered", this, nameof(RotateRight)); 
	   this.Connect("mouse_exited", this, nameof(RotateBack)); 
	   this.Connect("pressed", this, nameof(FullRotate));
	   this.RectPivotOffset = this.RectSize / 2;
	}

	public override void _Process(float delta) {
		this.RectRotation = currentRotation;
	}

	private void RotateRight() {
		if(tween.IsActive())
			tween.Stop(this, "currentRotation");
		tween.InterpolateProperty(this, "currentRotation", 0, endRotation, tweenTime, Tween.TransitionType.Linear,Tween.EaseType.Out);
		tween.Start();
	}

	private void RotateBack() {
		if(tween.IsActive())
			tween.Stop(this, "currentRotation");
		tween.InterpolateProperty(this, "currentRotation", endRotation, 0, tweenTime, Tween.TransitionType.Linear,Tween.EaseType.Out);
		tween.Start();
	}

	private void FullRotate() {
		if(tween.IsActive())
			tween.Stop(this, "currentRotation");
		tween.InterpolateProperty(this, "currentRotation", 0, endRotation * 4 + endRotation, tweenTime * 2, Tween.TransitionType.Linear,Tween.EaseType.Out);
		tween.Start();
	}

}
