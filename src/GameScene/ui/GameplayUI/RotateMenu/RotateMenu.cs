using Godot;
using System;

public class RotateMenu : Panel
{

	public event Action<int, int, int> OnRotatePlace = delegate { };

	private TextureButton placeButton;

	private Control arrowToRotate;

	private int x,y;
	private int rotation = 0;
	private int startingRotation;
	private Func<int,int,int,bool> RotationIsValid;

	public override void _Ready() {
		GetNode("GridContainer").GetNode<TextureButton>("LeftButton").Connect("pressed", this, nameof(RotateLeft));
		GetNode("GridContainer").GetNode<TextureButton>("RightButton").Connect("pressed", this, nameof(RotateRight));
		placeButton = GetNode("GridContainer").GetNode<TextureButton>("PlaceButton");
		placeButton.Connect("pressed", this, nameof(Place));
		var canvas = this.GetCanvasItem();
		VisualServer.CanvasItemSetZIndex(canvas, 3);
	}

	public override void _Input(InputEvent inputEvent) {
		if(this.Visible == false)
			return;
		if(inputEvent.IsActionPressed("rotate_left"))
			RotateLeft();	
		else if(inputEvent.IsActionPressed("rotate_right"))
			RotateRight();
		else if(inputEvent.IsActionPressed("ui_accept") && !placeButton.Disabled)
			Place();
	}

	public void Init(Func<int,int,int,bool> RotationIsValid) {
		this.RotationIsValid = RotationIsValid;
	}

	public void ActivateArrow(Control arrow, int x, int y) {
		ResetOld();
		arrowToRotate = arrow.GetNode<Control>("Arrow");
		rotation = (int) arrowToRotate.RectRotation;
		startingRotation = rotation;
		this.SetPosition(arrow.RectGlobalPosition + new Vector2(- arrow.GetParent<Control>().RectSize.x - 5, 120));
		this.Visible = true;
		this.x = x;
		this.y = y;
		TogglePlaceButton();
	}

	public void Deactivate() {
		ResetOld();
		this.Visible = false;
	}

	private void Place() {
		OnRotatePlace(x,y,rotation);
		arrowToRotate = null;
	}

	private void Rotate(int deg) {
		var arrow = arrowToRotate;
		arrow.RectRotation = arrow.RectRotation + deg;
		if(arrow.RectRotation > 180)
			arrow.RectRotation = arrow.RectRotation - 360;
		else if(arrow.RectRotation < -90)
			arrow.RectRotation = arrow.RectRotation + 360;
		rotation = (int) arrow.RectRotation; 
	}

	private void TogglePlaceButton() {
		placeButton.Disabled = !RotationIsValid(x,y,rotation); 
	}

	private void RotateLeft() {
		Rotate(-90);
		TogglePlaceButton();
	}

	private void RotateRight() {
		Rotate(90);
		TogglePlaceButton();
	}

	private void ResetOld() {
		if(arrowToRotate == null) {
			return;
		}
		arrowToRotate.RectRotation = startingRotation;
	}


}
