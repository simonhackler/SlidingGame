using Godot;
using SlidingGame.Logic;
using System.Collections.Generic;
using System;

public class MoveUi : Control {

	[Export]
	private Control moveToHighlight;

	private IMatchInteractable match;
	private TileScene[,] tiles;
	private Func<int> MyPlayerId;
	private List<TileScene> moveToTiles = new List<TileScene>();

	private int x,y;

	public void Init(TileScene[,] tiles, IMatchInteractable match, Func<int> MyPlayerId) {
		this.tiles = tiles;
		this.match = match;
		this.MyPlayerId =  MyPlayerId;
	}

	public void HighlightStone(int x, int y) {
		tiles[x,y].HighlightToMove();
		this.x = x;
		this.y = y;
		int gotoX = x+1;
		int gotoY = y;
		TrySetMoveToIcon(x,y,gotoX, gotoY);
		gotoX = x - 1;
		gotoY = y;
		TrySetMoveToIcon(x,y,gotoX, gotoY);
		gotoX = x;
		gotoY = y + 1;
		TrySetMoveToIcon(x,y,gotoX, gotoY);
		gotoX = x;
		gotoY = y - 1;
		TrySetMoveToIcon(x,y,gotoX, gotoY);
	}

	private void TrySetMoveToIcon(int x, int y, int gotoX, int gotoY) {
		if(match.CanMoveStoneTo(x,y, gotoX, gotoY,MyPlayerId())) {
			tiles[gotoX,gotoY].SetMoveTo();
			moveToTiles.Add(tiles[gotoX,gotoY]);
		}
	}

	public void UnHighlightStone() {
		tiles[x,y].UnHighlight();
		foreach(var tile in moveToTiles) {
			tile.UnsetMoveTo();
		}
		moveToTiles = new List<TileScene>();
	}

}
