using Godot;
using System;

public class GameOverMenu : Control {

	public event Action OnRematchButton = delegate { };
	public event Action OnQuitButton = delegate { };

	public override void _Ready() {
		var parent = GetNode("Panel").GetNode("VBoxContainer");
		parent.GetNode<Button>("RematchButton").Connect("pressed", this, nameof(RematchButtonPressed));
		parent.GetNode<Button>("QuitButton").Connect("pressed", this, nameof(QuitButtonPressed));
	}	

	public void SetGameOverText(string gameOverText) {
		GetNode("Panel").GetNode("VBoxContainer").GetNode<RichTextLabel>("RichTextLabel")
			.BbcodeText = $"[center]\n{gameOverText}[/center]";
	}

	private void RematchButtonPressed() {
		OnRematchButton();
	}

	private void QuitButtonPressed() {
		OnQuitButton();
	}

}
