using Godot;
using SlidingGame.Logic;
using System;
using NakamaWebrtcP2P.Logic;

public class Menus : CanvasLayer
{

	public event Action OnRematch = delegate { };
	public event Action OnConcede = delegate { };
	public event Action OnQuit = delegate { };

	private GameOverMenu gameOverMenu;
	private GameMenu gameMenu;
	private MatchConfig config;

	private IMatchInteractable match;
	private Func<int> MyPlayerId;


	public override void _Ready() {
		gameOverMenu = GetNode<GameOverMenu>("GameOverMenu");
		gameOverMenu.OnRematchButton += OnR;
		gameOverMenu.OnQuitButton += () => OnQuit();
		
		gameMenu = GetNode<GameMenu>("GameMenu");
		gameMenu.OnQuitButton += () => OnQuit();
		gameMenu.OnConcede += Concede;

		GetNode<Button>("MenuButton").Connect("pressed", this, nameof(ToggleGameMenu));
	}

	public void Init(IMatchInteractable match, MatchConfig config, Func<int> MyPlayerId) {
		this.match = match;
		this.config = config;
		this.MyPlayerId = MyPlayerId;
		match.OnGameOver += OnGameOver;
	}

	private void Concede() {
		OnConcede();
	}

	private void ToggleGameMenu() {
		gameMenu.Visible = !gameMenu.Visible;
	}

	private void OnGameOver(int x) {
		match.OnGameOver -= OnGameOver;
		gameOverMenu.Visible = true;
		string gameOverText;
		if(x == MyPlayerId()) {
			gameOverText = config.playerUsername + " won!";
		} else if(x == -1) {
			gameOverText = "Draw";
		} else {
			gameOverText = config.opponentUsername + " won!";
		}
		gameOverMenu.SetGameOverText(gameOverText);
		gameMenu.Visible = false;
		gameMenu.QueueFree();
		GetNode<Button>("MenuButton").QueueFree();
	}

	private void OnR() {
		OnRematch();
	}


}
