using Godot;
using System;

public class GameMenu : Control
{

	public event Action OnQuitButton = delegate { };
	public event Action OnConcede = delegate { };

	public override void _Ready() {
		GetNode("PanelContainer").GetNode("VBoxContainer").GetNode<Button>("QuitButton").Connect("pressed", this, nameof(QuitButton));
		GetNode("PanelContainer").GetNode("VBoxContainer").GetNode<Button>("ConcedeButton").Connect("pressed", this, nameof(ConcedeButton));
	}

	private void QuitButton() {
		OnQuitButton();
	}

	private void ConcedeButton() {
		OnConcede();
	}
 
	public override void _Input(InputEvent inputEvent) {
		if(inputEvent.IsActionPressed("ui_cancel")) {
			this.Visible = !this.Visible;
		}
	}

}
