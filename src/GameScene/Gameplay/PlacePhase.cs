using System;
using Godot;
using SlidingGame.Logic;
using System.Collections.Generic;

public class PlacePhase {

	public event Action<int,int> OnTileClicked = delegate { };

	private PackedScene arrowPrefab;
	private RotateMenu rotateMenu;
	private IMatchInteractable match;

	private TileScene[,] tiles;
	private Dictionary<(int,int), Control> mapObjects = new Dictionary<(int, int), Control>();

	private Func<int> MyPlayerId;

	public PlacePhase(PackedScene arrowPrefab, RotateMenu rotateMenu, IMatchInteractable match, TileScene[,] tiles, 
		Dictionary<(int,int), Control> mapObjects, Func<int> MyPlayerId) {

		this.arrowPrefab = arrowPrefab;
		this.rotateMenu = rotateMenu;
		this.match = match;
		this.tiles = tiles;
		this.mapObjects = mapObjects;
		this.MyPlayerId = MyPlayerId;
	}
	

	public void HandlePlacePhase(int x,int y) {
		if(rotateMenu.Visible) {
			return;
		}
		else if(!match.IsInStonePlacing && match.CanPlaceArrow(x,y,MyPlayerId())) {
			ActivateRotateMenu(x,y);
		} else if(match.CanPlace(x,y,MyPlayerId()) && match.IsInStonePlacing){
			OnTileClicked(x,y);
		}
	}

	private void ActivateRotateMenu(int x, int y) {
		var tile = tiles[x,y];
		var pos = tile.RectPosition;

		var arrow = arrowPrefab.Instance() as Control;
		mapObjects[(x,y)] = arrow;
		tile.Place(arrow);

		rotateMenu.ActivateArrow(arrow, x, y);
	}

}
