using Godot;
using System;
using System.Collections.Generic;
using SlidingGame.Logic;

public class MatchviewUpdater {


	private PackedScene stonePrefabP1;
	private PackedScene stonePrefabP2;
	private PackedScene arrowPrefab;
	private Dictionary<(int,int), Control> mapObjects = new Dictionary<(int, int), Control>();
	private TileScene[,] tiles;
	int player1Id;
	private Func<int> MyPlayerId;
	private IMatchInteractable match;
	private List<TileScene> toUnhighlight = new List<TileScene>();
	private List<TileScene> destroyedStoneTiles = new List<TileScene>();
	private TileScene rotationTile;

	private Queue<TileScene> targets = new Queue<TileScene>();

	public MatchviewUpdater(
			IMatchInteractable match, Dictionary<(int,int), Control> mapObjects, TileScene[,] tiles,
			PackedScene stonePrefabP1, PackedScene stonePrefabP2, int player1Id, Func<int> MyPlayerId, PackedScene arrowPrefab) {
				this.mapObjects = mapObjects;
				this.tiles = tiles;
				this.stonePrefabP1 = stonePrefabP1;
				this.stonePrefabP2 = stonePrefabP2;
				this.player1Id = player1Id;
				this.match = match;
				this.MyPlayerId = MyPlayerId;
				this.arrowPrefab = arrowPrefab;


				match.OnStonePlaced += PlaceStone;
				match.OnArrowPlaced += PlaceArrow;
				match.OnStoneMove += MoveStone;
				match.OnArrowRotated += RotateArrow;
				match.OnStoneDestroyed += DestroyStone;
				match.OnStoneInMiddle += StoneInMiddle;
				match.OnPhaseSwitch += HighlightPossible;
	}

	private void PlaceStone(int x, int y, int playerId) {
		Control stone;
		if(playerId == player1Id) {
			stone = stonePrefabP1.Instance() as Control;
		}
		else {
			stone = stonePrefabP2.Instance() as Control;
		}
		mapObjects.Add((x,y), stone);
		Place(x,y,stone);
	}

	private void Place(int x, int y, Control mapObj) {
		tiles[x,y].Place(mapObj);
		SetHighlight(x,y);
	}

	private void SetHighlight(int x, int y) {
		RemoveHighlights();
		tiles[x,y].HighlightPlaced();
		toUnhighlight.Add(tiles[x,y]);
	}

	private void RemoveHighlights() {
		foreach(var tile in toUnhighlight) {
			tile.UnHighlight();
		}
		toUnhighlight.Clear();
	}

	private void HighlightPossible() {
			foreach(var entry in mapObjects) {
				if(entry.Value.GetType() == typeof(ArrowHolder)) {
					tiles[entry.Key.Item1, entry.Key.Item2].HighlightTutorial();
					toUnhighlight.Add(tiles[entry.Key.Item1, entry.Key.Item2]);
				} else if(entry.Value.GetType() == typeof(StoneNode) && match.GetMapObj(entry.Key.Item1, entry.Key.Item2).PlayerId == match.CurrentPlayer()) {
					tiles[entry.Key.Item1, entry.Key.Item2].HighlightTutorial();
					toUnhighlight.Add(tiles[entry.Key.Item1, entry.Key.Item2]);
				}
			}
	}

	private void PlaceArrow(int x, int y, int rotation, int playerID) {
		SetHighlight(x,y);
		//TODO: This could be replaced by just checking if tiles[x,y] is already occupied
		if(match.CurrentPlayer() == MyPlayerId()) //Arrow is already placed
			return;
		Control arrow = arrowPrefab.Instance() as Control;
		arrow.GetNode<Control>("Arrow").RectRotation = rotation;
		Place(x,y,arrow);
		mapObjects[(x,y)] = arrow;
	}

	private void RotateArrow(int x, int y, int rotation, int playerId) {
		mapObjects[(x,y)].GetNode<Control>("Arrow").RectRotation = rotation;
		rotationTile = tiles[x,y];
		rotationTile.HighlightPlaced();
	}

	private void MoveStone(int start_x, int start_y,  List<(int,int)> locations, int playerId) {
		RemoveHighlights();
		toUnhighlight.AddRange(destroyedStoneTiles);
		if(rotationTile != null)
			toUnhighlight.Add(rotationTile);
		destroyedStoneTiles.Clear();
		int target_x = locations[locations.Count -1].Item1;
		int target_y = locations[locations.Count -1].Item2;
		var stone = (StoneNode) mapObjects[(start_x,start_y)];


		stone.Move(GetTargetTiles(locations, tiles));

		mapObjects.Remove((start_x, start_y));
		mapObjects.Add((target_x, target_y), stone);

		tiles[start_x,start_y].HighlightMovementStart();
		tiles[target_x, target_y].HighlightPlaced();
		toUnhighlight.Add(tiles[start_x,start_y]);
		toUnhighlight.Add(tiles[target_x,target_y]);
	}

	private static Queue<TileScene> GetTargetTiles(List<(int,int)> locations, TileScene[,] tiles) {
		var targets = new Queue<TileScene>();
		foreach(var location in locations) {
			targets.Enqueue(tiles[location.Item1, location.Item2]);
		}
		return targets;
	}

	private void DestroyStone(int x, int y) {
		var stone = mapObjects[(x,y)];
		mapObjects.Remove((x, y));
		stone.GetParent().RemoveChild(stone);
		stone.QueueFree();
		destroyedStoneTiles.Add(tiles[x,y]);
	}

	private void StoneInMiddle(int playerId) {
		int x = tiles.GetLength(1) / 2;
		int y = tiles.GetLength(1) / 2;
		var stone = mapObjects[(x,y)];
		((StoneNode) stone).DestroyOnWin(tiles[x,y]);
		mapObjects.Remove((x, y));
	}
}



