using Godot;
using SlidingGame.Logic;
using System;

public class HoverPreview {

	private Func<int> MyPlayerId;
	private IMatchInteractable match;
	private TileScene[,] tiles;
	private int previous_x, previous_y;
	private int player1Id;
	private Control stoneP1;
	private Control stoneP2;
	private Control arrow;
	private bool previewArrow;


	public HoverPreview(
			IMatchInteractable match, TileScene[,] tiles,
			PackedScene stonePrefabP1, PackedScene stonePrefabP2, int player1Id, Func<int> MyPlayerId, PackedScene arrowPrefab) {
		this.MyPlayerId = MyPlayerId;
		this.match = match;
		this.tiles = tiles;
		this.player1Id = player1Id;
		this.stoneP1 = stonePrefabP1.Instance() as Control;
		this.stoneP2 = stonePrefabP2.Instance() as Control;
		this.arrow = arrowPrefab.Instance() as Control;
		stoneP1.Modulate = new Color(1,1,1,0.5f);
		stoneP2.Modulate = new Color(1,1,1,0.5f);
		arrow.Modulate = new Color(1,1,1,0.5f);
		previewArrow = true;
		match.OnArrowPlaced += (x,y,z,w) => previewArrow = true;
		InitTiles(tiles);
	}

	private void InitTiles(TileScene[,] tiles) {
		foreach(var tile in tiles) {
			tile.OnTileHovered += PreviewMapObject;
			tile.OnTileClicked += BlockArrow;
		}
	}

	private void BlockArrow(int x, int y) {
		if(arrow.GetParent() != null) {
			arrow.GetParent().RemoveChild(arrow);
			previewArrow = false;
		}
	}

	private void PreviewMapObject(int x, int y) {
		if(match.InMovePhase())
			return;
		if(x == previous_x && y == previous_y)
			return;
		if(stoneP1.GetParent() != null)
			stoneP1.GetParent().RemoveChild(stoneP1);
		if(stoneP2.GetParent() != null)
			stoneP2.GetParent().RemoveChild(stoneP2);
		if(arrow.GetParent() != null)
			arrow.GetParent().RemoveChild(arrow);
		previous_x = x;
		previous_y = y;
		if(!match.CanPlace(x,y, MyPlayerId()))
			return;
		if(match.IsInStonePlacing)
			PreviewStone(x,y);
		else if(previewArrow && match.CanPlaceArrow(x,y,MyPlayerId()))
			PreviewArrow(x,y);
	}

	private void PreviewStone(int x, int y) {
		if(MyPlayerId() == player1Id) {
			tiles[x,y].Place(stoneP1);
		}
		else {
			tiles[x,y].Place(stoneP2);
		}
	}

	private void PreviewArrow(int x, int y) {
		tiles[x,y].Place(arrow);
	}

}
