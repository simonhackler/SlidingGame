using Godot;

public class ArrowHolder : Control
{
	public override void _Ready()
	{
		var arrow = GetNode<Control>("Arrow");
		var size = arrow.GetRect().Size;        
		arrow.RectPivotOffset = size / 2;
	}
}
