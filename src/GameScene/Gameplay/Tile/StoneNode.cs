using Godot;
using System.Collections.Generic;

public class StoneNode : Panel
{

	private Queue<TileScene> targets = new Queue<TileScene>();
	private TileScene destroyOnWinTarget;

	public override void _Ready() {
		var canvas = this.GetCanvasItem();
		VisualServer.CanvasItemSetZIndex(canvas, 2);
	}

	public override void _Process(float delta) {
		if(targets.Count > 0) {
			var target = targets.Peek().GetNode<Control>("Control").RectGlobalPosition + (targets.Peek().GetNode<Control>("Control").RectSize / 8); 
			this.RectGlobalPosition = this.RectGlobalPosition.MoveToward(target, delta * 750);
			if(this.RectGlobalPosition.DistanceTo(target) < 5) {
				targets.Dequeue();
			}
		}
		if(destroyOnWinTarget != null) {
			var target = destroyOnWinTarget.RectGlobalPosition + destroyOnWinTarget.RectSize / 8;
			if(this.RectGlobalPosition.DistanceTo(target) < 10) {
				QueueFree();
			}
		}
	}

	public void Move(Queue<TileScene> targets) {
		this.targets = targets;
	}

	public void DestroyOnWin(TileScene target) {
		destroyOnWinTarget = target;
	}

}
