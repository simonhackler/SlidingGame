using Godot;
using System;

public class TileScene : Panel
{

	public event Action<int,int> OnTileClicked = delegate { };
	public event Action<int,int> OnTileHovered = delegate { };

	private int x;
	private int y;

	public override void _Ready()
	{
		Connect("gui_input", this, nameof(OnClick));	
	}

	public void Init(int _x, int _y) {
		x = _x;
		y = _y;
	}

	public void Place(Control node) {
		GetNode("Control").AddChild(node);
	}

	public void HighlightTutorial() {
		GetNode<TextureRect>("Highlight").Modulate = new Color(0,0,0.8f,1);		
		GetNode<TextureRect>("Highlight").Visible = true;
		this.MouseDefaultCursorShape = CursorShape.PointingHand;
	}

	public void HighlightRemove() {
		GetNode<TextureRect>("Highlight").Modulate = new Color(0.74f,0,0,1);		
		GetNode<TextureRect>("Highlight").Visible = true;
	}

	public void HighlightMovementStart() {
		GetNode<TextureRect>("Highlight").Modulate = new Color(0,0.6f,0,1);		
		GetNode<TextureRect>("Highlight").Visible = true;
	}

	public void HighlightPlaced() {
		GetNode<TextureRect>("Highlight").Modulate = new Color(0,0.8f,0,1);		
		GetNode<TextureRect>("Highlight").Visible = true;
	}

	public void HighlightToMove() {
		GetNode<TextureRect>("Highlight").Modulate = new Color(0,1,0,1);		
		GetNode<TextureRect>("Highlight").Visible = true;
	}

	public void UnHighlight() {
		GetNode<TextureRect>("Highlight").Visible = false;
		this.MouseDefaultCursorShape = CursorShape.Arrow;
	}

	public void SetMoveTo() {
		GetNode<Control>("MoveTo").Visible = true;
	}

	public void UnsetMoveTo() {
		GetNode<Control>("MoveTo").Visible = false;
	}

	private void OnClick(InputEvent input) {
		OnTileHovered(x,y);
		if(!input.IsPressed())
			return;
		OnTileClicked(x,y);
	}

}
