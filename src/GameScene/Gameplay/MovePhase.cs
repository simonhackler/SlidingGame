using System;
using System.Collections.Generic;
using SlidingGame.Logic;
using Godot;

public class MovePhase {

	public event Action<int, int, int, int> OnMove = delegate { };
	public event Action<int, int, int> OnRotation = delegate { };

	private (int, int) selectedStonePos = (-1,-1);
	private Func<int> MyPlayerId;
	private Dictionary<(int,int), Control> mapObjects = new Dictionary<(int, int), Control>();
	private RotateMenu rotateMenu;
	private MoveUi moveUi;

	public MovePhase(Func<int> MyPlayerId, Dictionary<(int,int), Control> mapObjects, RotateMenu rotateMenu, MoveUi moveUi) {
		this.MyPlayerId = MyPlayerId;
		this.mapObjects = mapObjects;
		this.rotateMenu = rotateMenu;
		this.moveUi = moveUi;
	}

	public void HandleMovePhase(int x, int y, IMatchInteractable match) {
		if(selectedStonePos.Item1 != -1) {
			UnHighlightStone(mapObjects[(selectedStonePos.Item1, selectedStonePos.Item2)]);
			if(match.CanMoveStoneTo(selectedStonePos.Item1, selectedStonePos.Item2, x, y, MyPlayerId())) {
				OnMove(selectedStonePos.Item1, selectedStonePos.Item2, x, y);
				selectedStonePos = (-1, -1);
				return;
			} else {
				selectedStonePos = (-1, -1);
			}
		}
		MapObject mapObj = match.GetMapObj(x,y);
		if(mapObj == null) {
			rotateMenu.Deactivate();
			return;
		}
		else if(match.CanRotate(x,y,MyPlayerId())) {
			ArrowRotate(x,y);
			return;
		}
		else if(mapObj.GetType() == typeof(Stone) && mapObj.PlayerId == MyPlayerId()) {
			rotateMenu.Deactivate();
			SelectStone(x,y, (Stone) mapObj);
		}
	}

	private void SelectStone(int x, int y, Stone mapObj) {
		HighlightStone(mapObjects[(x,y)], x, y);
		selectedStonePos = (x,y);
	}

	private void HighlightStone(Control stone, int x, int y) {
		moveUi.HighlightStone(x,y);
	}

	private void UnHighlightStone(Control stone) {
		moveUi.UnHighlightStone();
	}

	private void ArrowRotate(int x, int y) {
		rotateMenu.ActivateArrow(mapObjects[(x,y)],x,y);
	}

}
