using Godot;
using System;
using System.Collections.Generic;
using SlidingGame.Logic;

public class GameScene : Control
{

	public event Action OnConcede = delegate { };
	public event Action<int,int> OnTileClicked = delegate { };
	public event Action<int,int,int> OnRotatePlaced = delegate { };
	public event Action<int, int, int, int> OnMove = delegate { };
	public event Action<int, int, int> OnRotation = delegate { };
	public event Action OnRematch = delegate { };
	public event Action OnQuit = delegate { };

	[Export]
	private PackedScene tilePrefab;
	[Export]
	private PackedScene stonePrefabP1;
	[Export]
	private PackedScene stonePrefabP2;
	[Export]
	private PackedScene arrowPrefab;
	[Export]
	private Texture kingTile;

	private PlacePhase placePhase;
	private MovePhase movePhase;
	private RotateMenu rotateMenu;

	private HoverPreview hoverPreview;

	private int size;
	private int player1Id;

	private IMatchInteractable match;
	private int myPlayerId;

	private Dictionary<(int,int), Control> mapObjects = new Dictionary<(int, int), Control>();
	private TileScene[,] tiles;

	public void StartMatch(IMatchInteractable match, MatchConfig config, int myPlayerId) {
		this.match = match;
		this.myPlayerId = myPlayerId;
		this.player1Id = config.player1;

		var grid = GetNode<GridContainer>("GridContainer");
		tiles = CreateMap(grid, config.size,tilePrefab, kingTile, OnTileClickedProp);

		rotateMenu = InitRotateMenu(match);

		var moveUi = GetNode<MoveUi>("MoveUi");
		moveUi.Init(tiles, match, () => this.myPlayerId);

		new MatchviewUpdater(match, mapObjects, tiles, stonePrefabP1, stonePrefabP2, player1Id, () => this.myPlayerId, arrowPrefab);
		movePhase = new MovePhase(() => this.myPlayerId, mapObjects, rotateMenu, moveUi);
		movePhase.OnMove += OnMove;

		hoverPreview = new HoverPreview(match, tiles, stonePrefabP1, stonePrefabP2, player1Id, () => this.myPlayerId, arrowPrefab);

		placePhase = new PlacePhase(arrowPrefab, rotateMenu, match, tiles, mapObjects, () => this.myPlayerId);
		placePhase.OnTileClicked += OnTileClicked;

		var menus = GetNode<Menus>("MenuLayer");
		menus.OnRematch += OnRematch;
		menus.OnQuit += OnQuit;
		menus.OnConcede += () => OnConcede();
		menus.Init(match, config, () => this.myPlayerId);

		GetNode("CanvasLayer").GetNode<GameHUD>("GameHUD").SetPlayerNames(config.playerUsername, config.opponentUsername, match, player1Id == myPlayerId, myPlayerId);
	}

	private RotateMenu InitRotateMenu(IMatchInteractable _match) {
		rotateMenu = GetNode<RotateMenu>("RotateMenu");
		rotateMenu.Visible = false;
		rotateMenu.OnRotatePlace += ArrowPlace;
		rotateMenu.Init(_match.RotationIsValid);
		return rotateMenu;
	}

	//TODO This seems very hacky to force local play into the existing design. It is however the quickest way for me to do this
	//This should probably be changed
	public void ChangePlayerID(int newId) {
		myPlayerId = newId;
	}

	//TODO This sucks. However I have no motivation right no to refactor this mess
	public void TutorialHighlight(int x, int y) {
		tiles[x,y].HighlightTutorial();
	}

	public void Unhighlight(int x, int y) {
		tiles[x,y].UnHighlight();
	}

	private static TileScene[,] CreateMap(
			GridContainer grid, int size, PackedScene tilePrefab, Texture kingTile, Action<int,int> OnTileClickedProp) {

		grid.Columns = size;
		var tiles = new TileScene[size,size];
		for(int i = 0; i < size; i++) {
			for(int j = 0; j < size; j++) {
				var tile = tilePrefab.Instance() as TileScene;
				tile.Name = "tile" + (i * size + j).ToString();
				tile.OnTileClicked += OnTileClickedProp;
				tile.Init(j,i);
				tiles[j,i] = tile;
				grid.AddChild(tile);
				if(j == size / 2 && i == size / 2) {
					tile.GetNode<TextureRect>("TextureRect").Texture = kingTile;
					tile.Name = "kingTile";
				}
			}
		}
		return tiles;
	}

	private void ArrowPlace(int x, int y, int rotation) {
		rotateMenu.Visible = false;
		if(match.InMovePhase())
			OnRotation(x,y,rotation);
		else
			OnRotatePlaced(x,y,rotation);
	}

	private void OnTileClickedProp(int x, int y) {
		if(match.CurrentPlayer() !=  myPlayerId) {
			return;
		}
		else if(match.InMovePhase()) {
			movePhase.HandleMovePhase(x, y, match);
		}
		placePhase.HandlePlacePhase(x,y);
	}

}
