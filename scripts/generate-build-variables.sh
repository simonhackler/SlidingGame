
#!/bin/bash

if [ -z "$NAKAMA_SERVER_KEY" -o -z "$NAKAMA_HOST" -o -z "$NAKAMA_PORT" ]; then
	exit 0
fi

echo "Doing the build thing"

#NAKAMA_SERVER_KEY=$(base64 -d <<< "$NAKAMA_SERVER_KEY")

# Use the Git hash if no CLIENT_VERSION is given.
if [ -z "$CLIENT_VERSION" ]; then
	# GitLab CI:
	if [ -n "$CI_COMMIT_SHA" ]; then
		CLIENT_VERSION="$CI_COMMIT_SHA"

	# GitHub Actions:
	elif [ -n "$GITHUB_SHA" ]; then
		CLIENT_VERSION="$GITHUB_SHA"
	fi
fi

cat << EOF > autoload/Build.gd
extends Node

var encryption_password := '$ENCRYPTION_PASSWORD'

func _ready() -> void:
	get_node("/root/Authenticator").ConfigureNakama("$NAKAMA_HOST", $NAKAMA_PORT, "$NAKAMA_SERVER_KEY", "https")

EOF

